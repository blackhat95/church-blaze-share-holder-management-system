<!-- /. NAV SIDE  -->
<div id="page-wrapper">
	<div id="page-inner">
		<!--BEGIN TITLE & BREADCRUMB PAGE-->
		<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
			<div class="page-header pull-left">
				<div class="page-title">
					System Settings
				</div>
			</div>
			<ol class="breadcrumb page-breadcrumb pull-right">
				<li><i class="fa fa-home"></i>&nbsp;<a href="<?php echo base_url() . "home" ?>">Home</a>&nbsp;&nbsp;
				</li>
				<li><i class="fa fa-gears"></i>&nbsp;&nbsp;<a href="<?php echo base_url() . 'settings/settingsHome' ?>">System
						Settings</a></li>
				<li class="active"><i class="fa fa-plus"></i>&nbsp;&nbsp;System Information</li>
			</ol>
			<div class="clearfix">
			</div>
		</div>
		<!--END TITLE & BREADCRUMB PAGE-->
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-info">
					<i class="fa fa-info-circle"></i>
					<strong>Heads up!</strong>
					This form helps with the addition of the system Infomation to be used as headers in the Reports!
				</div>
				<br/>
				<?php if (strlen($success) > 0) {
					?>
					<div class="alert alert-success" id="success"><i class="fa fa-check"></i>&nbsp;<?php echo $success
								. ''; ?>
					</div>
					<?php
				}
				?>
				<?php if (strlen($error) > 0) {
					?>
					<div class="alert alert-danger" id="error"><i class="fa fa-ban"></i>&nbsp;<?php echo $error . ''; ?>
					</div>
					<?php
				}
				?>
			</div>
		</div>
		<!-- /. ROW  -->
		<?php if ($view_data == null) { ?>
			<div class="row">
				<div class="col-md-12">
					<!-- Advanced Tables -->
					<div class="panel panel-pink">
						<div class="panel-heading">
							<div class="row">
								<div class="col-sm-6">
									<h3>Add System Infomartion</h3>
								</div>
							</div>
						</div>
						<div class="panel-body">
							<?php $this->load->helper('form'); ?>
							<?php echo form_open('settings/saveSystemInfo'); ?>
							<div class="form-body pal">
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label>System Name</label>
											<?php echo form_input(array ( "class" => "form-control",
													"placeholder" => "System Name", "name" => "name",
													"required" => "true" )) ?>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Address One</label>
											<?php echo form_input(array ( "class" => "form-control",
													"placeholder" => "Addres one", "name" => "addressone",
													"required" => "true" )) ?>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Address Two</label>
											<?php echo form_input(array ( "class" => "form-control",
													"placeholder" => "Address Two", "name" => "addresstwo" )) ?>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label>Phonenumber One</label>
											<?php echo form_input(array ( "class" => "form-control",
													"placeholder" => "Phonenumber One", "name" => "phoneone",
													"required" => "true" )) ?>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Phonenumber Two</label>
											<?php echo form_input(array ( "class" => "form-control",
													"placeholder" => "Phonenumber Two", "name" => "phonetwo" )) ?>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Bank Name</label>
											<?php echo form_input(array ( "class" => "form-control",
													"placeholder" => "Bank Name", "name" => "bankname",
													"required" => "true" )) ?>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label>Bank Branch</label>
											<?php echo form_input(array ( "class" => "form-control",
													"placeholder" => "Bank Branch", "name" => "branch",
													"required" => "true" )) ?>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Account Name</label>
											<?php echo form_input(array ( "class" => "form-control",
													"placeholder" => "Account Name", "name" => "accountname",
													"required" => "true" )) ?>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Account Number</label>
											<?php echo form_input(array ( "class" => "form-control",
													"placeholder" => "Account Number", "name" => "accountnumber",
													"required" => "true" )) ?>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label>Paybill Number</label>
											<?php echo form_input(array ( "class" => "form-control",
													"placeholder" => "Paybill Number", "name" => "paybill",
													"required" => "true" )) ?>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Added By</label>
											<?php echo form_input(array ( "class" => "form-control",
													"placeholder" => "Location", "name" => "addedby", "readonly" => "true",
													"value" => $this->session->userdata('name') )) ?>
										</div>
									</div>
									<div class="col-md-4">
									</div>
								</div>
								<hr/>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<button type="reset" class="btn btn-danger pull-right">Cancel</button>
											<?php echo form_submit('save', 'Add System Info',
													'class="btn btn-success pull-right margin-right"'); ?>

										</div>
									</div>
								</div>
							</div>
							</form>
							<!-- /.row (nested) -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
		<?php } ?>
		<?php if ($view_data != null) { ?>
			<?php header('Location:' . base_url('settings/viewSystemInfo')); ?>
		<?php } ?>
		<hr/>
