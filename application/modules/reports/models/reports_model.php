<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Reports_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function getShareholders() {
		$q = $this->db->select('*')->from('shareholders');
		$query = $q->get()->result();

		$result['rows'] = $query;

		return $result;
	}

	public function getShares($idnumber) {
		$q = $this->db->select('*')->from('shareholders')->join('sharesBought as sG', 'sG.shareholder = shareholders.Id')
				->where('idnumber', $idnumber);
		$query = $q->get()->result();

		$result['rows'] = $query;

		return $result;
	}

	public function get2($receipt) {
		$q = $this->db->select('*')->from('sharetypes')->join('payment', 'payment.shareType = sharetypes.Id')
				->where('buyreceipt', $receipt);

		$query = $q->get()->result();
		$result['rows'] = $query;
		return $result;

	}

	public function getSubscribedShares($dateone, $datetwo) {
		$q = $this->db->select('*')->from('shareholders')->join('sharesBought as sG', 'sG.shareholder = shareholders.Id')
				->where('sG.datebought >', $dateone)->where('sG.datebought <', $datetwo);
		$query = $q->get()->result();
		$result['rows'] = $query;
		return $result;
	}

	function totalSubscribedShares($dateone, $datetwo) {
		$result =
				mysql_query("SELECT SUM(shareNumber) AS shares_sum FROM sharesBought WHERE  datebought >= '$dateone' AND datebought <= '$datetwo'");
		$row = mysql_fetch_assoc($result);
		$totalShares = $row['shares_sum'];

		return $totalShares;
	}

	function totalAmountSubscribed($dateone, $datetwo) {
		$result =
				mysql_query("SELECT SUM(expectedAmount) AS expectedAmount FROM sharesBought WHERE datebought >= '$dateone' AND datebought <= '$datetwo'");
		$row = mysql_fetch_assoc($result);
		$totalAmount = $row['expectedAmount'];

		return $totalAmount;
	}

	public function getShareholdersDef($dateone, $datetwo) {
		$q = $this->db->select('*')->from('shareholders')->where('type =', 0)->where('dateadded >', $dateone)
				->where('dateadded <', $datetwo);
		$query = $q->get()->result();

		$result['rows'] = $query;

		return $result;
	}

	function totalShareholdersDef($dateone, $datetwo) {
		$q1 = $this->db->select('COUNT(*) as count', false)->from('shareholders')->where('type =', 0)
				->where('dateadded >=', $dateone)->where('dateadded <=', $datetwo);
		$tmp = $q1->get()->result();
		$result['row_count'] = $tmp[0]->count;

		return $result;
	}

	public function getCompanyShareholdersDef($dateone, $datetwo) {
		$q = $this->db->select('*')->from('shareholders')->where('type =', 1)->where('dateadded >', $dateone)
				->where('dateadded <', $datetwo);
		$query = $q->get()->result();

		$result['rows'] = $query;

		return $result;
	}

	function totalCompanyShareholdersDef($dateone, $datetwo) {
		$q1 = $this->db->select('COUNT(*) as count', false)->from('shareholders')->where('type =', 1)
				->where('dateadded >=', $dateone)->where('dateadded <=', $datetwo);
		$tmp = $q1->get()->result();
		$result['row_count'] = $tmp[0]->count;

		return $result;
	}

	public function getAllAgents() {
		$q = $this->db->select('*')->from('agents');
		$query = $q->get()->result();

		$result['rows'] = $query;

		return $result;
	}

	public function get_agentsdropdown() {
		$q = $this->db->select('*')->from('agents')->order_by('AgName');
		$query = $q->get()->result();
		$result['rows'] = $query;

		return $result;
	}

	public function getShareholderagent($dateone, $datetwo, $agentnumber) {
		$q = $this->db->select('*')->from('shareholders')->join('agents as ag', 'ag.Id = shareholders.agentID')
				->where('ag.Number =', $agentnumber)->where('shareholders.dateadded >', $dateone)
				->where('shareholders.dateadded <', $datetwo);
		$query = $q->get()->result();

		$result['rows'] = $query;

		return $result;
	}

	public function getTotalShareholderagent($dateone, $datetwo, $agentnumber) {
		$q = $this->db->select('COUNT(*) as count', false)->from('shareholders')
				->join('agents as ag', 'ag.Id = shareholders.agentID')->where('ag.Number =', $agentnumber)
				->where('shareholders.dateadded >', $dateone)->where('shareholders.dateadded <', $datetwo);
		$tmp = $q->get()->result();
		$result['row_count'] = $tmp[0]->count;

		return $result;
	}

	public function getAllShareholderAgent($agentnumber) {
		$q = $this->db->select('*')->from('shareholders')->join('agents as ag', 'ag.Id = shareholders.agentID')
				->where('ag.Number =', $agentnumber);
		$query = $q->get()->result();

		$result['rows'] = $query;

		return $result;
	}

	public function getAllTotalShareholderagent($agentnumber) {
		$q = $this->db->select('COUNT(*) as count', false)->from('shareholders')
				->join('agents as ag', 'ag.Id = shareholders.agentID')->where('ag.Number =', $agentnumber);
		$tmp = $q->get()->result();
		$result['row_count'] = $tmp[0]->count;

		return $result;
	}

	public function getShareRegister($dateone, $datetwo){

	}
}
