<!-- /. NAV SIDE  -->
<div id="page-wrapper">
	<div id="page-inner">
		<!--BEGIN TITLE & BREADCRUMB PAGE-->
		<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
			<div class="page-header pull-left">
				<div class="page-title">
					Staff Management
				</div>
			</div>
			<ol class="breadcrumb page-breadcrumb pull-right">
				<li><i class="fa fa-home"></i>&nbsp;<a href="<?php echo base_url() . "home" ?>">Home</a>&nbsp;&nbsp;
				</li>
				<li><i class="fa fa-user-secret"></i><a href="<?php echo base_url() . "users/dashboard" ?>">&nbsp;
						&nbsp;Staff Management</a></li>
				<li class="active"><i class="fa fa-plus"></i>&nbsp;&nbsp;Add Staff</li>
			</ol>
			<div class="clearfix">
			</div>
		</div>
		<!--END TITLE & BREADCRUMB PAGE-->
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-info">
					<i class="fa fa-info-circle"></i>
					<strong>Heads up!</strong>
					The addition of Staff into the system!
				</div>
				<br/>
				<?php if (strlen($success) > 0) {
					?>
					<div class="alert alert-success" id="success"><i class="fa fa-check"></i>&nbsp;<?php echo $success
								. ''; ?>
					</div>
					<?php
				}
				?>
				<?php if (strlen($error) > 0) {
					?>
					<div class="alert alert-danger" id="error"><i class="fa fa-ban"></i>&nbsp;<?php echo $error . ''; ?>
					</div>
					<?php
				}
				?>
			</div>
		</div>
		<!-- /. ROW  -->
		<div class="row">
			<div class="col-md-12">
				<!-- Advanced Tables -->
				<div class="panel panel-pink">
					<div class="panel-heading">
						<div class="row">
							<div class="col-sm-6">
								<h3>Add Churchblaze Staff</h3>
							</div>
						</div>
					</div>
					<div class="panel-body">
						<?php $this->load->helper('form'); ?>
						<?php echo form_open('staff/save'); ?>
						<div class="form-body pal">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label>Firstname</label>
										<?php echo form_input(array ( "class" => "form-control",
												"placeholder" => "First name", "name" => "fname", "required" => "true" )) ?>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Secondname</label>
										<?php echo form_input(array ( "class" => "form-control",
												"placeholder" => "Second name", "name" => "sname", "required" => "true" )) ?>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Lastname</label>
										<?php echo form_input(array ( "class" => "form-control",
												"placeholder" => "Last name", "name" => "lname", "required" => "true" )) ?>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label>Work Id</label>
										<?php echo form_input(array ( "class" => "form-control",
												"placeholder" => "Work number", "name" => "worknumber",
												"required" => "true" )) ?>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Department</label>
										<?php echo form_input(array ( "class" => "form-control",
												"placeholder" => "Department", "name" => "department",
												"required" => "true" )) ?>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Mobile Number</label>
										<?php echo form_input(array ( "class" => "form-control",
												"placeholder" => "Mobilenumber | Minimum length of 10 Characters",
												"name" => "mobile", "required" => "true" )) ?>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label>Email</label>
										<?php echo form_input(array ( "class" => "form-control",
												"placeholder" => "Email address", "name" => "email", "required" => "true",
												"type" => "email" )) ?>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Gender</label>
										<select class="form-control" id="gender" name="gender">
											<option value="Male">Male</option>
											<option value="Female">Female</option>
										</select>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Date of Birth</label>
										<?php echo form_input(array ( "class" => "form-control datepicker",
												"placeholder" => "Date of Birth. Format 1900-12-01", "name" => "dob",
												"id" => "dob", "required" => "true", "type" => "date" )) ?>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label>Id number</label>
										<?php echo form_input(array ( "class" => "form-control",
												"placeholder" => "Id Number", "name" => "idnumber",
												"required" => "true" )) ?>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Added By</label>
										<?php echo form_input(array ( "class" => "form-control", "placeholder" => "Location",
												"name" => "addedby", "readonly" => "true",
												"value" => $this->session->userdata('name') )) ?>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>User Roles</label>
										<select class="form-control" id="roles" name="role">
											<option value="Admin">Super Admin</option>
											<option value="Admin2">Admin</option>
											<option value="Finance">Finance</option>
										</select>
									</div>
								</div>
							</div>
							<hr/>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">

									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<button type="reset" class="btn btn-danger pull-right">Cancel</button>
										<?php echo form_submit('save', 'Add Staff',
												'class="btn btn-success pull-right margin-right"'); ?>

									</div>
								</div>
							</div>
						</div>
						</form>
						<!-- /.row (nested) -->
					</div>
					<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
