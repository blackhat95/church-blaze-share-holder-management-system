<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Reports extends MX_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('settings/settings_model', 'settings');
		$this->load->model('payment/payment_model', 'payment');
		$this->load->model('reports/reports_model', 'reports');
	}

	public function reportsHome() {
		if (($this->session->userdata('role') === "Admin" OR $this->session->userdata('role') === "Finance"
				OR $this->session->userdata('role') === "Admin2")
		) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['mainContent'] = 'reportsHome';
				$data['title'] = "General Reports";
				$data['view_data'] = '';

				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function viewShareholders() {
		if ($this->session->userdata('role') === "Admin" OR $this->session->userdata('role') === "Finance"
				OR $this->session->userdata('role') === "Admin2"
		) {
			if ($this->session->userdata('logged_in')) {
				$data['info'] = ("");
				$data['success'] = ("");
				$data['error'] = ("");
				$data['fields'] = array ( "physicalFormNumber" => "CB Form #", "Name" => "Full Names", "IdNo" => "Id Number",
						"krapin" => "KRA Pin", "Telephone" => "Mobile Number", "Email" => "Email", );
				$result = $this->reports->getShareholders();
				$data['view_data1'] = $this->settings->editSysInfo1();
				$data['mainContent'] = 'shareholderReport';
				$data['title'] = "All Share holders";
				$data['view_data'] = $result['rows'];
				if ($result['rows'] == null) {
					$data['error'] = ('No Shareholders were Found.');
				} else {
					$data['success'] = ('Shareholders were retrieved successfully');
				}
				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}

	public function viewShares() {
		if (($this->session->userdata('role') === "Admin") OR ($this->session->userdata('role') === "Admin2")
				OR ($this->session->userdata('role') === "Finance")
		) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['mainContent'] = 'individualShares';
				$data['title'] = "Individual Shares";
				$data['view_data'] = null;
				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}

	public function receipt() {
		if (($this->session->userdata('role') === 'Admin') OR ($this->session->userdata('role') === 'Admin2')
				OR ($this->session->userdata('role') === 'Finance') OR ($this->session->userdata('role') === 'Agent')
		) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['fields'] = array ( "physicalFormNumber" => "CB Receipt #", "Receiptnumber" => "Buy Code",
						"shareTypeName" => "Share Series", "shareNumber" => "Shares Bought",
						"expectedAmount" => "Expected Payment", "datebought" => "Date Bought",
						"Name" => "Shareholder Name" );
				$idnumber = $this->input->post('search');
				$result = $this->reports->getShares($idnumber);
				$data['view_data1'] = $this->settings->editSysInfo1();
				$data['mainContent'] = 'individualShares';
				$data['title'] = "Individual Shares";
				$data['view_data'] = $result['rows'];
				if ($result['rows'] == null) {
					$data['error'] = ('No Share subscription was found. Please Make sure that the user you are checking
					has subscribed shares');
				} else {
					$data['success'] = ('Share subscription was retrieved successfully');
				}
				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}

	public function viewPayment() {
		if (($this->session->userdata('role') === "Admin") OR ($this->session->userdata('role') === "Admin2")
				OR ($this->session->userdata('role') === "Finance")
		) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['mainContent'] = 'individualPayment';
				$data['title'] = "Individual Payment";
				$data['view_data'] = null;
				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}

	public function viewPaymentSearch() {
		if (($this->session->userdata('role') === 'Admin') OR ($this->session->userdata('role') === 'Finance')
				OR ($this->session->userdata('role') === 'Admin2')
		) {
			if ($this->session->userdata('logged_in')) {
				$data['info'] = ('');
				$data['success'] = ('');
				$data['error'] = ('');
				$data['fields'] = array ( "physicalFormNumber" => "CB Receipt #", 'PaymentUUID' => 'Unique Id',
						'Receiver' => 'Amount Receiver', 'PaymentType' => 'Payment Type', 'Amount' => 'Amount Paid',
						'numberofshares' => 'Share Paid For', 'date' => 'Payment Date', 'holderName' => 'Share Holder',
						'Code' => 'Payment Code' );
				$receipt = $this->input->post('receipt');
				$result = $this->reports->get2($receipt);
				$data['view_data1'] = $this->settings->editSysInfo1();
				$data['mainContent'] = 'individualPayment';
				$data['title'] = "Individual Payment";
				$data['view_data'] = $result['rows'];
				if ($result['rows'] == null) {
					$data['error'] = ('No share payment with the specified subscription receipt number was found');
				} else {
					$data['success'] = ('Share payment was retrieved successfully');
				}
				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function viewSubscribed() {
		if (($this->session->userdata('role') === "Admin") OR ($this->session->userdata('role') === "Admin2")
				OR ($this->session->userdata('role') === "Finance")
		) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['mainContent'] = 'subscribedShares';
				$data['title'] = "Subscribed Shares";
				$data['view_data'] = null;
				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}

	public function getSubscribedReport() {
		if (($this->session->userdata('role') === 'Admin') OR ($this->session->userdata('role') === 'Admin2')
				OR ($this->session->userdata('role') === 'Finance') OR ($this->session->userdata('role') === 'Agent')
		) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['fields'] = array ( "physicalFormNumber" => "CB Form #", "Receiptnumber" => "Buy Code",
						"shareTypeName" => "Share Series", "shareNumber" => "Shares Subscribed",
						"expectedAmount" => "Expected Payment", "datebought" => "Date Subscribed",
						"Name" => "Shareholder Name" );
				$dateone = $this->input->post('dateone');
				$datetwo = $this->input->post('datetwo');
				$result = $this->reports->getSubscribedShares($dateone, $datetwo);
				$sum = $this->reports->totalSubscribedShares($dateone, $datetwo);
				$sumAmount = $this->reports->totalAmountSubscribed($dateone, $datetwo);
				$data['view_data1'] = $this->settings->editSysInfo1();
				$data['mainContent'] = 'subscribedShares';
				$data['title'] = "Subscribed Shares";
				$data['view_data'] = $result['rows'];
				$data['totalSub'] = $sum;
				$data['totalAmount'] = $sumAmount;
				if ($result['rows'] == null) {
					$data['error'] = ('No Subscribed Shares were found for the dates defined.');
				} else {
					$data['success'] = ('Share subscription was retrieved successfully');
				}
				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}

	public function viewIndividualShareholdersReport() {
		if (($this->session->userdata('role') === "Admin") OR ($this->session->userdata('role') === "Admin2")
				OR ($this->session->userdata('role') === "Finance")
		) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['mainContent'] = 'individualShareholders';
				$data['title'] = "Share holders";
				$data['view_data'] = null;
				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}

	public function getIndividualShareholders() {
		if ($this->session->userdata('role') === "Admin" OR $this->session->userdata('role') === "Finance"
				OR $this->session->userdata('role') === "Admin2"
		) {
			if ($this->session->userdata('logged_in')) {
				$data['info'] = ("");
				$data['success'] = ("");
				$data['error'] = ("");
				$data['fields'] = array ( "physicalFormNumber" => "CB Form #", "Name" => "Full Names", "IdNo" => "Id Number",
						"Telephone" => "Mobile Number", "Email" => "Email", "dateadded" => "Date Added" );
				$dateone = $this->input->post('dateone');
				$datetwo = $this->input->post('datetwo');
				$result = $this->reports->getShareholdersDef($dateone, $datetwo);
				$resultCount = $this->reports->totalShareholdersDef($dateone, $datetwo);
				$data['shareholders'] = $resultCount['row_count'];
				$data['view_data1'] = $this->settings->editSysInfo1();
				$data['mainContent'] = 'individualShareholders';
				$data['title'] = "Share holders";
				$data['view_data'] = $result['rows'];
				if ($result['rows'] == null) {
					$data['error'] = ('No Individual Shareholders were found for the dates defined.');
				} else {
					$data['success'] = ('Individual Shareholders were retrieved successfully');
				}
				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}

	public function viewCompanyShareholdersReport() {
		if (($this->session->userdata('role') === "Admin") OR ($this->session->userdata('role') === "Admin2")
				OR ($this->session->userdata('role') === "Finance")
		) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['mainContent'] = 'companyShareholders';
				$data['title'] = "Share holders";
				$data['view_data'] = null;
				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}

	public function getCompanyShareholders() {
		if ($this->session->userdata('role') === "Admin" OR $this->session->userdata('role') === "Finance"
				OR $this->session->userdata('role') === "Admin2"
		) {
			if ($this->session->userdata('logged_in')) {
				$data['info'] = ("");
				$data['success'] = ("");
				$data['error'] = ("");
				$data['fields'] = array ( "physicalFormNumber" => "CB Form #", "Name" => "Full Names", "krapin" => "KRA PIN",
						"Telephone" => "Mobile Number", "Email" => "Email", "dateadded" => "Date Added" );
				$dateone = $this->input->post('dateone');
				$datetwo = $this->input->post('datetwo');
				$result = $this->reports->getCompanyShareholdersDef($dateone, $datetwo);
				$resultCount = $this->reports->totalCompanyShareholdersDef($dateone, $datetwo);
				$data['shareholders'] = $resultCount['row_count'];
				$data['view_data1'] = $this->settings->editSysInfo1();
				$data['mainContent'] = 'companyShareholders';
				$data['title'] = "Share holders";
				$data['view_data'] = $result['rows'];
				if ($result['rows'] == null) {
					$data['error'] = ('No Company Shareholders were found for the dates defined.');
				} else {
					$data['success'] = ('Company Shareholders were retrieved successfully');
				}
				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}

	public function viewAllAgents() {
		if ($this->session->userdata('role') === "Admin" OR $this->session->userdata('role') === "Finance"
				OR $this->session->userdata('role') === "Admin2"
		) {
			if ($this->session->userdata('logged_in')) {
				$data['info'] = ("");
				$data['success'] = ("");
				$data['error'] = ("");
				$data['fields'] = array ( "Number" => "Agent Number", "AgName" => "Full Names", "IDNo" => "Id Number",
						"Location" => "Location", "Telephone" => "Mobile Number", "Email" => "Email", );
				$result = $this->reports->getAllAgents();
				$data['view_data1'] = $this->settings->editSysInfo1();
				$data['mainContent'] = 'allAgentsReport';
				$data['title'] = "All Agents";
				$data['view_data'] = $result['rows'];
				if ($result['rows'] == null) {
					$data['error'] = ('No Agents were Found.');
				} else {
					$data['success'] = ('Agents were retrieved successfully');
				}
				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}

	public function agentsView() {
		if (($this->session->userdata('role') === "Admin") OR ($this->session->userdata('role') === "Admin2")
				OR ($this->session->userdata('role') === "Finance")
		) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['mainContent'] = 'agentsShareholders';
				$data['title'] = "Agents Share Holders";
				$data['view_data'] = null;
				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}

	public function getAgentsSubscription() {
		if ($this->session->userdata('role') === "Admin" OR $this->session->userdata('role') === "Finance"
				OR $this->session->userdata('role') === "Admin2"
		) {
			if ($this->session->userdata('logged_in')) {
				$data['info'] = ("");
				$data['success'] = ("");
				$data['error'] = ("");
				$data['fields'] = array ( "physicalFormNumber" => "CB Form #", "Name" => "Full Names", "IdNo" => "Id Number",
						"krapin" => "KRA PIN", "Telephone" => "Mobile Number", "dateadded" => "Date Added" );
				$dateone = $this->input->post('dateone');
				$datetwo = $this->input->post('datetwo');
				$agentnumber = $this->input->post('agent');
				if ($agentnumber == null && $dateone == null && $datetwo == null) {
					$data['mainContent'] = 'agentsShareholders';
					$data['title'] = "Agents Share Holders";
					$result = $this->reports->get_agentsdropdown();
					$data['agents'] = $result['rows'];
					$data['view_data'] = null;
					$data['error'] = ('Please Fill In the sapces to receive Results.');
					$this->load->view('Admin/adminRedirect', $data);
				} elseif ($agentnumber == null) {
					$data['mainContent'] = 'agentsShareholders';
					$data['title'] = "Agents Share Holders";
					$result = $this->reports->get_agentsdropdown();
					$data['agents'] = $result['rows'];
					$data['view_data'] = null;
					$data['error'] = ('The agent Number cannot be left as Empty.');
					$this->load->view('Admin/adminRedirect', $data);
				} else {
					if ($dateone == null && $datetwo == null) {
						$results = $this->reports->getAllShareholderAgent($agentnumber);
						$resultCount = $this->reports->getAllTotalShareholderagent($agentnumber);
					} else {
						$results = $this->reports->getShareholderagent($dateone, $datetwo, $agentnumber);
						$resultCount = $this->reports->getTotalShareholderagent($dateone, $datetwo, $agentnumber);
					}
					$data['shareholders'] = $resultCount['row_count'];
					$data['view_data1'] = $this->settings->editSysInfo1();
					$data['mainContent'] = 'agentsShareholders';
					$data['title'] = "Agents Share Holders";
					$data['view_data'] = $results['rows'];
					if ($results['rows'] == null) {
						$data['error'] = ('No Company Shareholders were found for the dates defined.');
					} else {
						$data['success'] = ('Company Shareholders were retrieved successfully');
					}
					$this->load->view('Admin/adminRedirect', $data);
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}

	public function agentsAddedOverTime() {
		if ($this->session->userdata('role') == "Admin" || $this->session->userdata('role') == "Admin2"
				|| $this->session->userdata('role') == "Finance"
		) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['mainContent'] = 'agentsOverTime';
				$data['title'] = "Agents Over Time";
				$data['view_data'] = null;
				$this->load->view('Admin/adminRedirect', $data);
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}

	public function share_register() {
		$role = $this->session->userdata('role');
		if ($role == 'Admin' || $role == 'Admin2' || $role == 'Finance') {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = '';
				$data['error'] = '';
				$data['info'] = '';
				$data['mainContent'] = 'shareRegister';
				$data['title'] = 'Share register';
				$data['view_data'] = null;
				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		}
	}
}
