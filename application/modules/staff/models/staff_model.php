<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Staff_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function get($limit, $offset, $sort_by, $sort_order) {
		$sort_order = ($sort_order == 'desc') ? 'desc' : 'asc';
		$sort_columns = array ( 'Fname', 'Sname', 'Lname', 'WorkNumber', 'Department', 'Gender', 'Email', 'DOB', 'Mobile' );
		$sort_by = (in_array($sort_by, $sort_columns)) ? $sort_by : 'Fname';

		$q = $this->db->select('*')->from('staff as workers')->join('users', 'users.link2 = workers.Id')
				->limit($limit, $offset)->order_by($sort_by, $sort_order);

		$query = $q->get()->result();

		$q = $this->db->select('COUNT(*) as count', false)->from('staff');
		$tmp = $q->get()->result();

		$result['rows'] = $query;
		$result['row_count'] = $tmp[0]->count;

		return $result;

	}

	public function getsearch($index) {
		$q = $this->db->select('*')->from('staff')->where('staff.WorkNumber', $index)
				->join('users', 'users.link2 = staff.Id');
		$query = $q->get()->result();

		$result['rows'] = $query;

		return $result;
	}

	public function editGet($id = null) {
		$this->db->select('*');
		$this->db->from('staff as workers');
		$this->db->join('users', 'users.link2 = workers.Id');

		if ($id != null) {
			$this->db->where(array ( 'workers.Id' => $id ));
		}
		$query = $this->db->get();
		if ($id != null) {
			return $query->row_array();
		}
	}

	public function remove($id) {
		$this->db->where('Id', $id);
		$this->db->delete('staff');
	}

	public function add($index) {
		if (isset($index['Id'])) {
			$this->db->where('Id', $index['Id']);
			$this->db->update('staff', $index);
		} else {
			$this->db->insert('staff', $index);
		}

	}

	public function addUser($user) {
		if (isset($user['id'])) {
			$this->db->where('id', $user['id']);
			$this->db->update('users', $user);
		} else {
			$this->db->insert('users', $user);
		}

	}

	function getidnumber($idnumber = null) {
		$this->db->select('*')->from('staff')->where('idnumber', $idnumber);
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->idnumber;
		}
	}
}
