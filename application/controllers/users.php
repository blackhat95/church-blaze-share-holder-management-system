<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Users extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('user_model');
		$this->load->model('admin_model');

		$this->load->model('users_model');

	}

	public function index() {

		$this->load->library('session');

		$this->load->helper('url');

		$data['profile'] = $this->user_model->get_user($this->session->userdata('username'));

		$this->load->view('includes/header');
		$this->load->view('includes/menu', $data);
		$this->load->view('users/profile', $data);
		$this->load->view('includes/footer');
		$this->load->view('includes/datatables');

	}

	public function avatar() {

		$this->load->library('session');

		$this->load->helper('url');

		$data['profile'] = $this->user_model->get_user($this->session->userdata('username'));

		$this->load->view('includes/header');
		$this->load->view('includes/menu', $data);
		$this->load->view('users/avatar', $data);
		$this->load->view('includes/footer');
		$this->load->view('includes/datatables');

	}

	public function password() {

		$this->load->library('session');

		$this->load->helper('url');

		$data['profile'] = $this->user_model->get_user($this->session->userdata('username'));

		$this->load->view('includes/header');
		$this->load->view('includes/menu', $data);
		$this->load->view('users/password', $data);
		$this->load->view('includes/footer');
		$this->load->view('includes/datatables');

	}

	//logs out the user and re initializes the session varibles
	function logout() {
		$this->load->library('session');
		$this->users_model->logout($this->session->userdata('username'));
		$newdata = array ( 'logged_in' => false );

		$this->session->set_userdata($newdata);
		$data['success'] = ("");
		$this->load->helper(array ( 'form', 'url' ));
		$this->load->library('form_validation');
		$this->form_validation->set_rules('user', 'Username ', 'required');
		$this->form_validation->set_rules('pass', 'Password  ', 'required');

		if ($this->form_validation->run() == false) {
			// $this->load->view('includes/header');
			$this->load->view('login', $data);
		} else {
			$passw = $this->users_model->logindetails();
			if ($this->input->post("pass") == $passw) {
				$this->load->library('session');
				$newdata = array ( 'username' => $this->input->post("user"), 'logged_in' => true );

				$this->session->set_userdata($newdata);
				$users = $this->session->all_userdata();
				$this->users_model->login($this->input->post("user"));
				$this->dashboard();
			} else {
				$data['success'] = ("Login Failed !");
				//  $this->load->view('includes/header');
				$this->load->view('login', $data);

			}

		}

	}

	//allows you to edit the details of a user
	function edituser() {
		$this->load->helper(array ( 'form', 'url' ));

		$this->load->library('form_validation');

		$this->form_validation->set_rules('name', 'Fullname ', 'required');
		$this->form_validation->set_rules('username', 'username ', 'required');

		if ($this->form_validation->run() == false) {
			echo "0";
		} else {
			$this->load->library('session');
			$this->users_model->edituser();
			echo "1";
		}
	}

	function editpatientdetails() {
		$this->load->library('session');
		$this->load->helper(array ( 'form', 'url' ));
		$this->load->library('form_validation');

		$this->form_validation->set_rules('patient_id', 'Patient Id ', 'required|number');
		$this->form_validation->set_rules('patient_name', "Patients' Name", 'required');
		$this->form_validation->set_rules('patient_gender', "Patients' Gender", 'required');
		$this->form_validation->set_rules('patient_dob', "Patients' DOB", 'required');
		$this->form_validation->set_rules('patient_email', "Patients' Email", 'required|valid_email');
		$this->form_validation->set_rules('patient_phone', "Patients' Phone", 'required');
		$this->form_validation->set_rules('patient_nationality', "Patients' Nationality", 'required');
		$this->form_validation->set_rules('patient_nextofkin', "Applicants' Next of Kin", 'required');
		$this->form_validation->set_rules('patient_nextofkincontact', "Next of Kin Contacts", 'required');

		if ($this->form_validation->run() == false) {

			echo 0;
		} else {

			$this->users_model->editpatientdetails();
			echo 1;

		}
	}

	function patient_edit($patient_id) {
		$this->load->library('session');
		$this->load->helper(array ( 'form', 'url' ));
		$this->load->library('form_validation');

		$this->form_validation->set_rules('patient_id', 'Patient Id ', 'required');
		$this->form_validation->set_rules('patient_name', "Patients' Name", 'required');
		$this->form_validation->set_rules('patient_gender', "Patients' Gender", 'required');
		$this->form_validation->set_rules('patient_dob', "Patients' DOB", 'required');
		$this->form_validation->set_rules('patient_email', "Patients' Email", 'required|valid_email');
		$this->form_validation->set_rules('patient_phone', "Patients' Phone", 'required');
		$this->form_validation->set_rules('patient_nationality', "Patients' Nationality", 'required');
		$this->form_validation->set_rules('patient_nextofkin', "Applicants' Next of Kin", 'required');
		$this->form_validation->set_rules('patient_nextofkincontact', "Next of Kin Contacts", 'required');

		if ($this->form_validation->run() == false) {
			echo "0";
		} else {
			$this->users_model->patient_edit($patient_id);
			echo "1";
		}

	}

	function edit_history($patient_id) {

		$this->load->library('session');
		$this->load->helper(array ( 'form', 'url' ));
		$this->load->library('form_validation');

		$this->form_validation->set_rules('patient_insuarance', "Patient' Skills", 'required');
		$this->form_validation->set_rules('relevantinformation', "Relevant Information", 'required');

		if ($this->form_validation->run() == false) {

			echo "0";
		} else {
			$this->users_model->edit_history($patient_id);
			echo "1";
		}

	}

	function edit_patient($patient_id) {
		$this->load->library('session');
		$this->load->helper(array ( 'form', 'url' ));
		$this->load->library('form_validation');

		$data['patient'] = $this->users_model->get_patient($patient_id);

		if ($this->session->userdata('logged_in') == "TRUE") {
			$data['profile'] = $this->user_model->get_user($this->session->userdata('username'));

			$this->load->view('includes/header');
			$this->load->view('includes/menu', $data);
			$this->load->view('users/edit_patient', $data);
			$this->load->view('includes/footer');
			$this->load->view('includes/datatables');

			$this->load->view('includes/wizard');

		} else {

			$data['success'] = ("Login Required to  Edit Patient");
			$this->load->helper(array ( 'form', 'url' ));
			$this->load->library('form_validation');
			$this->form_validation->set_rules('username', 'Username ', 'required');
			$this->form_validation->set_rules('password', 'Password  ', 'required');

			if ($this->form_validation->run() == false) {

				$this->load->view('includes/header');
				$this->load->view('login', $data);
				$this->load->view('includes/footer');
				$this->load->view('includes/datatables');

			}

		}

	}

	function editpatient() {
		$this->load->library('session');
		$this->load->helper(array ( 'form', 'url' ));
		$this->load->library('form_validation');

		if ($this->session->userdata('logged_in') == "TRUE") {
			$data['profile'] = $this->user_model->get_user($this->session->userdata('username'));
			$data['patients'] = $this->users_model->get_patients();

			$this->load->view('includes/header');
			$this->load->view('includes/menu', $data);
			$this->load->view('users/patient_details', $data);
			$this->load->view('includes/footer');
			$this->load->view('includes/datatables');

			$this->load->view('includes/wizard');

		} else {

			$data['success'] = ("Login Required to Add Patient");
			$this->load->helper(array ( 'form', 'url' ));
			$this->load->library('form_validation');
			$this->form_validation->set_rules('username', 'Username ', 'required');
			$this->form_validation->set_rules('password', 'Password  ', 'required');

			if ($this->form_validation->run() == false) {

				$this->load->view('includes/header');
				$this->load->view('login', $data);
				$this->load->view('includes/footer');
				$this->load->view('includes/datatables');

			}

		}

	}

	function addpatient() {

		$this->load->library('session');
		$this->load->helper(array ( 'form', 'url' ));

		if ($this->session->userdata('logged_in') == "TRUE") {
			$data['profile'] = $this->user_model->get_user($this->session->userdata('username'));

			$this->load->view('includes/header');
			$this->load->view('includes/menu', $data);
			$this->load->view('users/add_patient', $data);
			$this->load->view('includes/footer');
			$this->load->view('includes/datatables');

			$this->load->view('includes/wizard');

		} else {

			$data['success'] = ("Login Required to Add Patient");
			$this->load->helper(array ( 'form', 'url' ));
			$this->load->library('form_validation');
			$this->form_validation->set_rules('username', 'Username ', 'required');
			$this->form_validation->set_rules('password', 'Password  ', 'required');

			if ($this->form_validation->run() == false) {

				$this->load->view('includes/header');
				$this->load->view('login', $data);
				$this->load->view('includes/footer');
				$this->load->view('includes/datatables');

			}

		}

	}

	function editpatienthistory($patient_id) {

		$this->load->library('session');

		$this->load->helper(array ( 'form', 'url' ));
		$this->load->library('form_validation');

		$this->form_validation->set_rules('patient_insuarance', "Patient' Skills", 'required');
		$this->form_validation->set_rules('relevantinformation', "Relevant Information", 'required');

		if ($this->form_validation->run() == false) {

			echo "0";

		} else {
			$this->users_model->editpatienthistory($patient_id);
			echo "1";
		}

	}

	//change both avatars or one of them
	function changeavatars() {
		$this->load->library('session');

		$this->load->helper(array ( 'form', 'url' ));

		$config['upload_path'] = './assets/img/users/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '10000';
		$config['max_width'] = '10240';
		$config['max_height'] = '7680';
		$config['overwrite'] = false;
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		$useravatar = 'useravatar';

		if ($this->upload->do_upload($useravatar)) {
			$this->load->library('session');
			//   $error = $this->upload->display_errors();
			$this->users_model->changeuseravatar();

			echo "1";
		} else {
			$error = $this->upload->display_errors();
			echo $error;
			echo "0";
		}

	}

}

