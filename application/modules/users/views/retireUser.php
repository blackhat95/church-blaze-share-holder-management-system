<!-- /. NAV SIDE  -->
<div id="page-wrapper">
	<div id="page-inner">
		<!--BEGIN TITLE & BREADCRUMB PAGE-->
		<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
			<div class="page-header pull-left">
				<div class="page-title">
					Users Management
				</div>
			</div>
			<ol class="breadcrumb page-breadcrumb pull-right">
				<li><i class="fa fa-home"></i>&nbsp;<a href="<?php echo base_url() . "home" ?>">Home</a>&nbsp;&nbsp;
				</li>
				<li><i class="fa fa-user-secret"></i><a href="<?php echo base_url() . "users/dashboard" ?>">&nbsp;&nbsp;users
						Managementt</a></li>
				<li class="active"><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;Retire / Active System Users</li>
			</ol>
			<div class="clearfix">
			</div>
		</div>
		<!--END TITLE & BREADCRUMB PAGE-->
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-info">
					<i class="fa fa-info-circle"></i>
					<strong>Heads up!</strong>
					All the users are rited through this form
				</div>
				<br/>
				<?php if (strlen($success) > 0) {
					?>
					<div class="alert alert-success" id="success"><i class="fa fa-check"></i>&nbsp;<?php echo $success
								. ''; ?>
					</div>
					<?php
				}
				?>
				<?php if (strlen($error) > 0) {
					?>
					<div class="alert alert-danger" id="error"><i class="fa fa-ban"></i>&nbsp;<?php echo $error . ''; ?>
					</div>
					<?php
				}
				?>
				<?php if (strlen($info) > 0) {
					?>
					<div class="alert alert-info" id="info"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $info . ''; ?>
					</div>
					<?php
				}
				?>
			</div>
		</div>
		<!-- /. ROW  -->
		<div class="row">
			<div class="col-md-12">
				<!-- Advanced Tables -->
				<div class="panel panel-pink">
					<div class="panel-heading">
						<strong>System Users</strong>
					</div>
					<div class="panel-body">
						<?php $this->load->helper('form'); ?>
						<?php echo form_open('users/updateStatus'); ?>
						<div class="form-body pal">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label>Name</label>
										<?php echo form_input(array ( "class" => "form-control", "value" => $name,
												"name" => "name", "id" => "name", "readonly" => "true" )) ?>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Username</label>
										<?php echo form_input(array ( "class" => "form-control",
												"value" => $view_data['username'], "name" => "username", "id" => "username",
												"readonly" => "true" )) ?>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Role</label>
										<?php echo form_input(array ( "class" => "form-control",
												"value" => $view_data['role'], "name" => "role", "id" => "role",
												"readonly" => "true" )) ?>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Date Created</label>
										<?php echo form_input(array ( "class" => "form-control",
												"value" => $view_data['datecreated'], "name" => "datecreated",
												"id" => "datecreated", "readonly" => "true" )) ?>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Status</label>
										<select class="form-control" id="status" name="status">
											<option value="0">Retire</option>
											<option value="1">Activate</option>
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<div class="form-group">
											<div class="input-icon right">
												<?php echo form_hidden('password', $view_data['password'],
														'class="form-control"'); ?>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<div class="input-icon right">
											<?php echo form_hidden('id', $view_data['id'], 'class="form-control"'); ?>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<button type="reset" class="btn btn-danger pull-right">Cancel</button>
										<?php echo form_submit('updateStatus', 'Save',
												'class="btn btn-success pull-right margin-right"'); ?>

									</div>
								</div>
							</div>
						</div>
						</form>
					</div>
					<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
