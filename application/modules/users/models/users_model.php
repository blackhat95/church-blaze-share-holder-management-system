<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Users_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function get($limit, $offset, $sort_by, $sort_order) {
		$sort_order = ($sort_order == 'desc') ? 'desc' : 'asc';
		$sort_columns = array ( 'username', 'role', 'status', 'datecreated' );
		$sort_by = (in_array($sort_by, $sort_columns)) ? $sort_by : 'username';

		$q = $this->db->select('*')->from('users')->limit($limit, $offset)->order_by($sort_by, $sort_order);

		$query = $q->get()->result();

		$q = $this->db->select('COUNT(*) as count', false)->from('users');
		$tmp = $q->get()->result();

		$result['rows'] = $query;
		$result['row_count'] = $tmp[0]->count;

		return $result;
	}

	public function remove($id) {
		$this->db->where('id', $id);
		$this->db->delete('users');
	}

	public function add($index) {
		if (isset($index['id'])) {
			$this->db->where('id', $index['id']);
			$this->db->update('users', $index);
		} else {
			$this->db->insert('users', $index);
		}

	}

	public function edit($id = null) {
		$this->db->select('*');
		$this->db->from('users');

		if ($id != null) {
			$this->db->where(array ( 'users.id' => $id ));
		}
		$query = $this->db->get();
		if ($id != null) {
			return $query->row_array();
		}
	}

	function getrole($id = null) {
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where(array ( 'id' => $id ));
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->role;
		}
	}

	function getStaff($id = null, $role) {
		$this->db->select('*');
		$this->db->from('staff');
		$this->db->join('users', 'users.link2 = staff.Id');
		$this->db->where(array ( 'users.id' => $id, 'users.role' => $role ));
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			$name = $row->Fname . " " . $row->Sname . " " . $row->Lname;
			return $name;
		}
	}

	function getAgent($id = null, $role) {
		$this->db->select('*');
		$this->db->from('agents');
		$this->db->join('users', 'users.link = agents.Id');
		$this->db->where(array ( 'users.id' => $id, 'users.role' => $role ));
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->AgName;
		}
	}

	function getShareholder($id = null, $role) {
		$this->db->select('*');
		$this->db->from('shareholders');
		$this->db->join('users', 'users.link3 = shareholders.Id');
		$this->db->where(array ( 'users.id' => $id, 'users.role' => $role ));
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->Name;
		}
	}

	public function getsearch($index) {
		$q = $this->db->select('*')->from('users')->where('username', $index);
		$query = $q->get()->result();

		$result['rows'] = $query;

		return $result;
	}

}
