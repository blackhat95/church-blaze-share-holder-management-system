<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Agents extends MX_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('agents/agents_model', 'agents');
		$this->load->model('shareholders/shareholders_model', 'shareholders');
	}

	public function dashboard() {
		if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Finance")
				OR ($this->session->userdata('role') == "Admin2")
		) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['mainContent'] = 'agentsDashboard';
				$data['title'] = "Agents Dashboard";
				$data['view_data'] = '';

				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function viewAgents($sort_by = 'AgName', $sort_order = 'asc', $offset = 0) {
		if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Finance")
				OR ($this->session->userdata('role') == "Admin2")
		) {
			if ($this->session->userdata('logged_in')) {
				$data['info'] = ("");
				$data['success'] = ("");
				$data['error'] = ("");
				$limit = 5;
				$data['fields'] = array ( "AgName" => "Agent Name", "Number" => "Agent #", "accountNumber" => "Account #",
						"IDNo" => "Agent Id #", "Location" => "Location", "Telephone" => "Mobile #", "Email" => "Email" );
				$data['headers'] = array ( "AgName" => "Agent Name", "Number" => "Agent #", "accountNumber" => "Account #",
						"IDNo" => "Agent Id #", "Location" => "Location", "Telephone" => "Mobile #", "Email" => "Email" );
				$result = $this->agents->get($limit, $offset, $sort_by, $sort_order);
				$index = null;
				$result2 = $this->agents->getsearch($index);
				$data['view_data2'] = $result2['rows'];
				$data['mainContent'] = 'viewAgents';
				$data['title'] = "View Agents";
				$data['view_data'] = $result['rows'];
				$data['rownumber'] = $result['row_count'];
				//pagination
				$this->load->library('pagination');
				$config = array ();
				$config['base_url'] = site_url("agents/viewAgents/$sort_by/$sort_order/");
				$config['total_rows'] = $data['rownumber'];
				$config['per_page'] = $limit;
				$config['uri_segment'] = 5;
				$this->pagination->initialize($config);
				$data['pagination'] = $this->pagination->create_links();

				$data['sort_by'] = $sort_by;
				$data['sort_order'] = $sort_order;
				$data['info'] = ("Agents Loaded Successfully!");
				log_message('info', 'User:- ' . $this->session->userdata('name') . ' viewed agents.');
				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function search($sort_by = 'Id', $sort_order = 'asc', $offset = 0) {
		if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Admin2")) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$limit = 15;
				$data['fields'] =
						array ( "AgName" => "Agent Name", "Number" => "Agent Number", "accountNumber" => "Account #",
								"IDNo" => "Agent Id Number", "Location" => "Location", "Telephone" => "Mobile Number",
								"Email" => "Email" );
				$data['headers'] = array ( "AgName" => "Agent Name", "Number" => "Agent #", "accountNumber" => "Account #",
						"IDNo" => "Agent Id #", "Location" => "Location", "Telephone" => "Mobile #", "Email" => "Email" );

				$result = $this->agents->get($limit, $offset, $sort_by, $sort_order);
				$index = $this->input->post('search');
				$result2 = $this->agents->getsearch($index);
				$data['info'] = ("");
				$data['success'] = ("");
				$data['error'] = ("");
				$data['mainContent'] = 'viewAgents';
				$data['title'] = "View Agents";
				$data['view_data2'] = $result2['rows'];
				$data['view_data'] = $result['rows'];
				$data['rownumber'] = $result['row_count'];
				if ($result2['rows'] == null) {
					$data['error'] = ("Not agent with the specified Id number/ Agent Number was found");
				} else {
					$data['success'] = ("Agents Loaded Successfully!");
				}
				//pagination
				$this->load->library('pagination');
				$config = array ();
				$config['base_url'] = site_url("agents/search/$sort_by/$sort_order/");
				$config['total_rows'] = $data['rownumber'];
				$config['per_page'] = $limit;
				$config['uri_segment'] = 5;
				$this->pagination->initialize($config);
				$data['pagination'] = $this->pagination->create_links();

				$data['sort_by'] = $sort_by;
				$data['sort_order'] = $sort_order;

				$this->load->view('Admin/adminRedirect', $data);
				log_message('info', 'User:- ' . $this->session->userdata('name') . ' searched for the agent ' . $index);
				$this->load->view('Admin/includes/datatables');;
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}

	public function addAgents() {
		if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Finance")
				OR ($this->session->userdata('role') == "Admin2")
		) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['mainContent'] = 'addAgents';
				$data['title'] = "Add Agents";
				$data['view_data'] = '';

				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function modify($id = null) {
		if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Finance")
				OR ($this->session->userdata('role') == "Admin2")
		) {
			if ($this->session->userdata('logged_in')) {
				if ($id == null) {
					show_error('No shit found', 500);
				} else {
					$index['success'] = ("");
					$index['error'] = ("");
					$index['info'] = ("");
					$index['mainContent'] = 'editAgents';
					$index['title'] = "Edit Agents";
					$index['view_data'] = $this->agents->editGet($id);

					$this->load->view('Admin/adminRedirect', $index);
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function remove($id = null) {
		if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Finance")
				OR ($this->session->userdata('role') == "Admin2")
		) {
			if ($this->session->userdata('logged_in')) {
				if ($id == null) {
					show_error('No shit provided', 500);
				} else {
					$this->agents->remove($id);
					$data['success'] = ("");
					$data['error'] = ("");
					$data['info'] = ("");
					$data['success'] = ("Staff deleted successfully");
					log_message('info', 'User:- ' . $this->session->userdata('name') . ' deleted the agent ' . $id);
					header('Location:' . base_url('index.php/agents/viewAgents', $data));
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function save() {
		if (($this->session->userdata('role') === "Admin") OR ($this->session->userdata('role') === "Finance")
				OR ($this->session->userdata('role') === "Admin2")
		) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['info'] = ("");
				$this->load->library('form_validation');
				$this->form_validation->set_rules('name', 'Agent Name', 'required|trim');
				$this->form_validation->set_rules('idnumber', 'Id Number', 'required|trim');
				$this->form_validation->set_rules('location', 'Location', 'required|trim');
				$this->form_validation->set_rules('account', 'Agent Account Number', 'required|trim');
				$this->form_validation->set_rules('mobile', 'Mobile number', 'required|trim|min_length[10]');
				$this->form_validation->set_rules('email', 'Email address', 'required|trim|valid_email');
				$this->form_validation->set_rules('username', 'Username', 'required|trim');
				$this->form_validation->set_message('is_unique', "This email has already been taken");
				$agentnumber = $this->agents->agentNumberGen();
				$agentName = $this->input->post('name');
				$username = $this->input->post('username');
				$exist = $this->shareholders->getusername($username);
				if ($this->form_validation->run()) {
					if ($exist != $username) {
						if (isset($_POST) && $_POST['save'] === 'Add Agents') {
							$index['AgName'] = $this->input->post('name');
							$index['IDNo'] = $this->input->post('idnumber');
							$index['Location'] = $this->input->post('location');
							$index['accountNumber'] = $this->input->post('account');
							$index['Number'] = $agentnumber;
							$index['Telephone'] = $this->input->post('mobile');
							$index['Email'] = $this->input->post('email');
							$index['AddedBy'] = $this->input->post('addedby');
							$index['dateadded'] = date("Y-m-d");
							$this->agents->add($index);
							$user['username'] = $username;
							$user['password'] = md5("password");
							$user['role'] = $this->input->post('role');
							$user['link'] = $this->db->insert_id();
							$this->agents->addUser($user);
							$data['success'] = ("");
							$data['error'] = ("");
							$data['info'] = ("");
							$data['mainContent'] = 'addAgents';
							$data['title'] = "Add Agents";
							$data['view_data'] = '';
							$data['success'] = ("Agent was added successfully");
							log_message('info',
									'User:- ' . $this->session->userdata('name') . ' added the agent ' . $agentName);
							$this->load->view('Admin/adminRedirect', $data);
						} else {
							$data['success'] = ("");
							$data['error'] = ("");
							$data['info'] = ("");
							$data['mainContent'] = 'addAgents';
							$data['title'] = "Add Agents";
							$data['view_data'] = '';
							$data['error'] = ("Agent Could not be registered");
							log_message('info', 'info',
									'User:- ' . $this->session->userdata('name') . ' could not add the agent ' . $agentName);
							$this->load->view('Admin/adminRedirect', $data);
						}
					} else {
						$data['success'] = ("");
						$data['error'] = ("");
						$data['info'] = ("");
						$data['mainContent'] = 'addAgents';
						$data['title'] = "Add Agents";
						$data['view_data'] = '';
						log_message('info', 'The username ' . $username . ' has been taken. Please choose a new one.');
						$data['error'] = ("The choosen username is already taken. Please choose a new one");

						$this->load->view('Admin/adminRedirect', $data);
					}
				} else {
					$data['success'] = ("");
					$data['error'] = ("");
					$data['info'] = ("");
					$data['mainContent'] = 'addAgents';
					$data['title'] = "Add Agents";
					$data['view_data'] = '';
					$data['error'] = ("Please fill all the spaces appropriately");
					log_message('info', 'Please fill in the all the spaces appropriately');
					$this->load->view('Admin/adminRedirect', $data);
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}

	public function update($id = null) {
		if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Finance")
				OR ($this->session->userdata('role') == "Admin2")
		) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['info'] = ("");
				$this->load->library('form_validation');
				$this->form_validation->set_rules('name', 'Agent Name', 'required|trim');
				$this->form_validation->set_rules('number', 'Agent Number', 'required|trim');
				$this->form_validation->set_rules('idnumber', 'Id Number', 'required|trim');
				$this->form_validation->set_rules('location', 'Location', 'required|trim');
				$this->form_validation->set_rules('account', 'Agent Account Number', 'required|trim');
				$this->form_validation->set_rules('mobile', 'Mobile number', 'required|trim|min_length[10]');
				$this->form_validation->set_rules('email', 'Email address', 'required|trim|valid_email');
				$this->form_validation->set_rules('username', 'Username', 'required|trim');
				$this->form_validation->set_message('is_unique', "This email has already been taken");
				$agentName = $this->input->post('name');
				$username = $this->input->post('username');
				if ($this->form_validation->run()) {
					if (isset($_POST) && $_POST['update'] == 'Update') {
						$index['Id'] = $this->input->post('Id');
						$index['AgName'] = $this->input->post('name');
						$index['IDNo'] = $this->input->post('idnumber');
						$index['Location'] = $this->input->post('location');
						$index['accountNumber'] = $this->input->post('account');
						$index['Number'] = $this->input->post('number');
						$index['Telephone'] = $this->input->post('mobile');
						$index['Email'] = $this->input->post('email');
						$index['AddedBy'] = $this->input->post('addedby');
						$index['dateadded'] = $this->input->post('dateadded');
						$index['UpdatedBy'] = $this->input->post('editedby');
						$index['dateupdated'] = date("Y-m-d");
						$this->agents->add($index);
						$user['id'] = $this->input->post('id');
						$user['username'] = $this->input->post('username');
						$user['password'] = md5("password");
						$user['role'] = $this->input->post('role');
						$user['link'] = $this->input->post('Id');
						$this->agents->addUser($user);
						$data['success'] = ("");
						$data['error'] = ("");
						$data['info'] = ("");
						$data['mainContent'] = 'editAgents';
						$data['title'] = "Edit Agents";
						$data['view_data'] = $this->agents->editGet();
						$data['success'] = ("Agents was updated successfully");
						log_message('info', 'info',
								'User:- ' . $this->session->userdata('name') . ' updated the agent ' . $agentName);
						$this->load->view('Admin/adminRedirect', $data);
					} else {
						$data['success'] = ("");
						$data['error'] = ("");
						$data['info'] = ("");
						$data['mainContent'] = 'editAgents';
						$data['title'] = "Edit Agents";
						$data['view_data'] = $this->agents->editGet();
						$data['error'] = ("Agents Could not be updated");
						log_message('info', 'info',
								'User:- ' . $this->session->userdata('name') . 'could not update the agent ' . $agentName);
						$this->load->view('Admin/adminRedirect', $data);
					}
				} else {
					$data['success'] = ("");
					$data['error'] = ("");
					$data['info'] = ("");
					$data['mainContent'] = 'editAgents';
					$data['title'] = "Edit Agents";
					$index['view_data'] = $this->agents->get($id);
					$data['error'] = ("Please fill all the spaces appropriately");
					log_message('info', 'info', 'Please fill all the spaces appropriately');
					$this->load->view('Admin/adminRedirect', $data);
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

}
