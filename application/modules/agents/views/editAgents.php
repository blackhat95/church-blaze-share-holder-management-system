<!-- /. NAV SIDE  -->
<div id="page-wrapper">
	<div id="page-inner">
		<!--BEGIN TITLE & BREADCRUMB PAGE-->
		<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
			<div class="page-header pull-left">
				<div class="page-title">
					Agents
				</div>
			</div>
			<ol class="breadcrumb page-breadcrumb pull-right">
				<li><i class="fa fa-home"></i>&nbsp;<a href="<?php echo base_url() . "home" ?>">Home</a>&nbsp;&nbsp;
				</li>
				<li><i class="fa fa-user-md"></i><a href='<?php echo base_url() . 'agents/dashboard' ?>'>&nbsp;
						&nbsp;Agents Management</a></li>
				<li class="active"><i class="fa fa-edit"></i>&nbsp;&nbsp;Edit Agents</li>
			</ol>
			<div class="clearfix">
			</div>
		</div>
		<!--END TITLE & BREADCRUMB PAGE-->
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-info">
					<i class="fa fa-info-circle"></i>
					<strong>Heads up!</strong>
					This form helps with the editing of agents in the system!
				</div>
				<br/>
				<?php if (strlen($success) > 0) {
					?>
					<div class="alert alert-success" id="success"><i class="fa fa-check"></i>&nbsp;<?php echo $success
								. ''; ?>
					</div>
					<?php
				}
				?>
				<?php if (strlen($error) > 0) {
					?>
					<div class="alert alert-danger" id="error"><i class="fa fa-ban"></i>&nbsp;<?php echo $error . ''; ?>
					</div>
					<?php
				}
				?>
			</div>
		</div>
		<!-- /. ROW  -->
		<div class="row">
			<div class="col-md-12">
				<!-- Advanced Tables -->
				<div class="panel panel-pink">
					<div class="panel-heading">
						<div class="row">
							<div class="col-sm-6">
								<h3>Edit Churchblaze Agents</h3>
							</div>
						</div>
					</div>
					<div class="panel-body">
						<?php $this->load->helper('form'); ?>
						<?php echo form_open('agents/update'); ?>
						<div class="form-body pal">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label>Agents Name</label>
										<?php echo form_input(array ( "class" => "form-control",
												"placeholder" => "Agent Name", "name" => "name",
												"value" => $view_data['AgName'], "required" => "true" )) ?>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Agent Number</label>
										<?php echo form_input(array ( "class" => "form-control",
												"placeholder" => "Agent Number", "name" => "number",
												"value" => $view_data['Number'], "readonly" => "true" )) ?>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Id Number</label>
										<?php echo form_input(array ( "class" => "form-control",
												"placeholder" => "Id Number", "name" => "idnumber",
												"value" => $view_data['IDNo'], "required" => "true" )) ?>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label>Location</label>
										<?php echo form_input(array ( "class" => "form-control", "placeholder" => "Location",
												"name" => "location", "value" => $view_data['Location'],
												"required" => "true" )) ?>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Mobile Number</label>
										<?php echo form_input(array ( "class" => "form-control",
												"placeholder" => "Mobilenumber | Minimum length of 10 Characters",
												"name" => "mobile", "value" => $view_data['Telephone'],
												"required" => "true" )) ?>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Email</label>
										<?php echo form_input(array ( "class" => "form-control",
												"placeholder" => "Email address", "name" => "email",
												"value" => $view_data['Email'], "required" => "true", "type" => "email" )) ?>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label>Agent Account Number</label>
										<?php echo form_input(array ( "class" => "form-control",
												"placeholder" => "Agent Account Number", "name" => "account",
												"value" => $view_data['accountNumber'], "required" => "true",
												"type" => "text" )) ?>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Added By</label>
										<?php echo form_input(array ( "class" => "form-control", "placeholder" => "Location",
												"name" => "addedby", "readonly" => "true",
												"value" => $view_data['AddedBy'] )) ?>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Edited By</label>
										<?php echo form_input(array ( "class" => "form-control", "placeholder" => "Location",
												"name" => "editedby", "readonly" => "true",
												"value" => $this->session->userdata('name') )) ?>
									</div>
								</div>
							</div>
							<hr/>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Username</label>
										<?php echo form_input(array ( "class" => "form-control", "placeholder" => "Username",
												"name" => "username", "value" => $view_data['username'],
												"required" => "true" )) ?>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Role</label>
										<?php echo form_input(array ( "class" => "form-control", "placeholder" => "Role",
												"name" => "role", "value" => "Agent", "readonly" => "true" )) ?>
									</div>
								</div>
							</div>
							<hr/>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<div class="input-icon right">
											<?php echo form_hidden('Id', $view_data['Id'], 'class="form-control"'); ?>
											<?php echo form_hidden('dateadded', $view_data['dateadded'],
													'class="form-control"'); ?>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<div class="input-icon right">
											<?php echo form_hidden('id', $view_data['id'], 'class="form-control"'); ?>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<button type="reset" class="btn btn-danger pull-right">Cancel</button>
										<?php echo form_submit('update', 'Update',
												'class="btn btn-success pull-right margin-right"'); ?>

									</div>
								</div>
							</div>
						</div>
						</form>
						<!-- /.row (nested) -->
					</div>
					<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
