<!-- /. NAV SIDE  -->
<div id="page-wrapper">
	<div id="page-inner">
		<!--BEGIN TITLE & BREADCRUMB PAGE-->
		<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb no-print">
			<div class="page-header pull-left">
				<div class="page-title">
					Shareholder Management
				</div>
			</div>
			<ol class="breadcrumb page-breadcrumb pull-right no-print">
				<li><i class="fa fa-home"></i>&nbsp;<a href="<?php echo base_url() . "home" ?>">Home</a>&nbsp;&nbsp;
				</li>
				<li><i class="fa fa-users"></i><a href='<?php echo base_url() . 'shareholders/shareholderAdd' ?>'>&nbsp;&nbsp;Shareholder
						Management</a></li>
				<li class="active"><i class="fa fa-print"></i>&nbsp;&nbsp;User Details Report</li>
			</ol>
			<div class="clearfix">
			</div>
		</div>
		<!--END TITLE & BREADCRUMB PAGE-->
		<div class="row">
			<div class="col-md-12">
				<!-- Advanced Tables -->
				<?php if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Finance")
				OR ($this->session->userdata('role') == "Admin2")) { ?>
				<div class="panel panel-report">
					<?php } ?>
					<?php if (($this->session->userdata('role') == "Agent")) { ?>
					<div class="panel panel-report">
						<?php } ?>
						<div class="panel-heading no-print">
							<div class="row">
								<div class="col-sm-6">
									<h3>User Details Report</h3>
								</div>
							</div>
						</div>
						<div class="panel-body">
							<?php $this->load->helper('form'); ?>
							<?php echo form_open('shares/save'); ?>
							<div class="form-body pal">
								<div class="row">
									<h2 class="center receiptheader"><?php echo $view_data1['Name'] ?></h2>

									<h3 class="center receiptheader"><?php echo $view_data1['addressone'] ?>
										,&nbsp;<?php echo $view_data1['addresstwo'] ?></h3>

									<h3 class="center receiptheader">Phonenumber
										:-&nbsp;<?php echo $view_data1['phonenumberone'] ?>
										,&nbsp;<?php echo $view_data1['phonenumbertwo'] ?></h3>
								</div>
								<hr/>
								<div class="row">
									<h3 class="center heading"><b>Shareholder Summary</b></h3>

									<div class="col-md-6">
										<div class="form-group">
											<h4 class="pull-left"><b>S/N(CB Physical Form):</b>&nbsp;<b
														class="text-red"><?php echo $view_data['physicalFormNumber'] ?></b>
											</h4>
										</div>
									</div>
									<div class="col-md-6">
										<h4 class="pull-right"><b>S/N:</b>&nbsp;<b
													class="text-red"><?php echo $view_data['formnumber'] ?></b></h4>
									</div>
								</div>
								<div class="row">
									<table class="table">
										<tr>
											<td><p class="pull-left">Shareholder: &nbsp;<b
															class="heading"><?php echo $view_data['Name'] ?></b>&nbsp;
												</p></td>
											<td>
												<?php if ($view_data['IdNo'] != null) { ?>
													<p>Id Number: &nbsp;<b
																class="heading"><?php echo $view_data['IdNo'] ?></b>
													</p>
												<?php } ?>
												<?php if ($view_data['krapin'] != null) { ?>
													<p>KRA Pin Number: &nbsp;<b
																class="heading"><?php echo $view_data['krapin'] ?></b>
													</p>
												<?php } ?>
											</td>
											<td><p class="pull-right">Date Registered: &nbsp;<b
															class="heading"><?php echo $view_data['dateadded'] ?></b>&nbsp;
												</p></td>
										</tr>
									</table>
									<br/>

									<div class="table-responsive">
										<h4 class="center heading"><b>Shareholder Contacts</b></h4>
										<table class="table table-condensed" id="stafftable">
											<thead>
											<tr>
												<th>Country</th>
												<th>County</th>
												<th>Town</th>
												<th>Vilalage</th>
												<th>Tel. No</th>
												<th>Email</th>
												<th>Postal Address</th>
											</tr>
											</thead>
											<tbody>
											<tr>
												<td><?php echo $view_data['Country'] ?></td>
												<td><?php echo $view_data['County'] ?></td>
												<td><?php echo $view_data['Town'] ?></td>
												<td><?php echo $view_data['Village'] ?></td>
												<td><?php echo $view_data['Telephone'] ?></td>
												<td><?php echo $view_data['Email'] ?></td>
												<td><?php echo $view_data['Box'] ?></td>
											</tr>
											</tbody>
										</table>
									</div>
									<br/>
									<?php if ($view_data['type'] == 0) { ?>
										<div class="table-responsive">
											<h4 class="center heading"><b>Shareholder Next of Kins</b></h4>
											<table class="table table-condensed" id="stafftable">
												<thead>
												<tr>
													<th>Name</th>
													<th>ID.NO / B/C No</th>
													<th>Phone No</th>
													<th>Percentage</th>
													<th>Relation</th>
												</tr>
												</thead>
												<tbody>
												<?php foreach ($kins as $key => $data): ?>
													<tr>
														<td><?php echo $data->kinName ?></td>
														<td><?php echo $data->kinIdentity ?></td>
														<td><?php echo $data->kinPhonenumber ?></td>
														<td><?php echo $data->percentage ?></td>
														<td><?php echo $data->relation ?></td>
													</tr>
												<?php endforeach; ?>
												</tbody>
											</table>
										</div>
									<?php } ?>
									<br/>

									<div class="table-responsive">
										<h4 class="center heading"><b>Shareholder Registration Payment</b></h4>
										<table class="table table-condensed" id="stafftable">
											<thead>
											<tr>
												<th>Receipt No.</th>
												<th>Payment Mode</th>
												<th>code</th>
												<th>Amount</th>
												<th>Date Paied</th>
											</tr>
											</thead>
											<tbody>
											<?php foreach ($view_data5 as $key => $data): ?>
												<tr>
													<td><?php echo $data->receipt ?></td>
													<td><?php echo $data->paymentmode ?></td>
													<td><?php echo $data->code ?></td>
													<td><?php echo $data->amount ?></td>
													<td><?php echo $data->datepaid ?></td>
												</tr>
											<?php endforeach; ?>
											</tbody>
										</table>
									</div>
									<div class="table-responsive">
										<h4 class="center heading"><b>Shareholder Shares Subscribed</b></h4>
										<table class="table table-condensed" id="stafftable">
											<thead>
											<tr>
												<th>Receipt Number</th>
												<th>Series</th>
												<th>Number of Shares</th>
												<th>Expected Amount</th>
												<th>Date Subscribed</th>
											</tr>
											</thead>
											<tbody>
											<?php foreach ($view_data6 as $key => $data): ?>
												<tr>
													<td><?php echo $data->Receiptnumber ?></td>
													<td><?php echo $data->shareTypeName ?></td>
													<td><?php echo $data->shareNumber ?></td>
													<td><?php echo $data->expectedAmount ?></td>
													<td><?php echo $data->datebought ?></td>
												</tr>
											<?php endforeach; ?>
											</tbody>
										</table>
									</div>
									<br/>

									<div class="table-responsive">
										<h4 class="center heading"><b>Shareholder Shares Payments</b></h4>
										<table class="table table-condensed" id="stafftable">
											<thead>
											<tr>
												<th>Payment Receipt</th>
												<th>Subscription Receipt</th>
												<th>Payment Mode</th>
												<th>Code</th>
												<th>Amount Paid</th>
												<th>Number of shares</th>
												<th>Date Paid</th>
											</tr>
											</thead>
											<tbody>
											<?php foreach ($view_data4 as $key => $data): ?>
												<tr>
													<td><?php echo $data->PaymentUUID ?></td>
													<td><?php echo $data->buyreceipt ?></td>
													<td><?php echo $data->PaymentType ?></td>
													<td><?php echo $data->Code ?></td>
													<td><?php echo $data->Amount ?></td>
													<td><?php echo $data->numberofshares ?></td>
													<td><?php echo $data->date ?></td>
												</tr>
											<?php endforeach; ?>
											</tbody>
										</table>
									</div>
									<div class="table-responsive">
										<table class="table table-condensed" id="stafftable">
											<tbody>
											<tr>
												<td><p class="pull-left">Agent: <b class="heading">
															&nbsp;<?php echo $view_data2['AgName'] ?></b>&nbsp;
													</p>
												</td>
												<td><p class="pull-left">Id No / Psst No:<b class="heading">
															&nbsp;<?php echo $view_data2['IDNo'] ?></b>&nbsp;</p></td>
												<td><p class="pull-right">Agent: <b class="heading">
															&nbsp;<?php echo $view_data2['Telephone'] ?></b>&nbsp;
													</p>
												</td>
											</tr>
											</tbody>
										</table>
									</div>
									<div class="table-responsive">
										<table class="table table-condensed" id="stafftable">
											<tbody>
											<tr>
												<td><p class="pull-left">
														Recommender:Name.....................................</p>
												</td>
												<td><p class="pull-left">Id No / Psst No:..................................
												</td>
												<td><p class="pull-right">Phone No:......................................</p>
												</td>
											</tr>
											</tbody>
										</table>
									</div>
								</div>
								<div class="row ">
									<div class="col-md-12">
										<table class="table table-condensed">
											<tr>
												<td class="pull-left col-md-6"><p><b>Signature:....................................................</b>
													</p></td>
												<td class="pull-right col-md-6"><p><b>Date:.....................................................</b>
													</p></td>
											</tr>
										</table>
										<table class="table table-condensed">
											<tr>
												<td class="pull-left col-md-6"><p><b>Signature:....................................................</b>
													</p></td>
												<td class="pull-right col-md-6"><p><b>Date:.....................................................</b>
													</p></td>
											</tr>
										</table>
									</div>
								</div>
								<div class="row ">
									<div class="col-md-6">
										<p><b>Terms & Conditions</b></p>
										<ol>
											<li>Applicant must remain with a copy of the application form, pay-in-slip and
												original cash receipt
											</li>
										</ol>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">

										<p><b>NB:</b>&nbsp;All cash must be deposited in the following back account</p>
										<ul>
											<li><b><?php echo $view_data1['bankname'] ?></b></li>
											<li><b><?php echo $view_data1['bankbranch'] ?></b></li>
											<li><b>A/C Name: <?php echo $view_data1['accountname'] ?></b></li>
											<li><b>A/C NO.<?php echo $view_data1['accountnumber'] ?></b></li>
											<li><b>Paybill Number: <?php echo $view_data1['paybillnumber'] ?></b></li>
										</ul>
									</div>
								</div>
								<hr/>
								<div class="row no-print">
									<div class="col-md-4">
										<div class="form-group">
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<a class="btn btn-bitbucket pull-right" href="javascript:window.print();"><i
														class="fa fa-print"></i>&nbsp;Print
											</a>
										</div>
									</div>
								</div>
							</div>
							</form>
							<!-- /.row (nested) -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
