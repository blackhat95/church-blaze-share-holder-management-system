<div id="page-wrapper">
	<!--BEGIN TITLE & BREADCRUMB PAGE-->
	<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
		<div class="page-header pull-left">
			<div class="page-title">
				User Profile
			</div>
		</div>
		<ol class="breadcrumb page-breadcrumb pull-right">
			<li><i class="fa fa-home"></i>&nbsp;<a href="<?php echo base_url() . "home" ?>">Home</a>&nbsp;&nbsp;
			</li>
			<li><i class="fa fa-users"></i>&nbsp;&nbsp; User Management</li>
			<li class="active"><i class="fa fa-user-times"></i>&nbsp;&nbsp; user profile</li>
		</ol>
		<div class="clearfix">
		</div>
	</div>
	<!--END TITLE & BREADCRUMB PAGE-->
	<div class="row">
		<div class="col-md-12">
			<div class="alert alert-info">
				<i class="fa fa-info-circle"></i>
				<strong>Heads up!</strong>
				This helps the user in some administrative settings of the system!
			</div>
			<br/>
			<?php if (strlen($success) > 0) {
				?>
				<div class="alert alert-success" id="success"><i class="fa fa-check"></i>&nbsp;<?php echo $success
						. ''; ?>
				</div>
				<?php
			}
			?>
			<?php if (strlen($error) > 0) {
				?>
				<div class="alert alert-danger" id="error"><i class="fa fa-ban"></i>&nbsp;<?php echo $error . ''; ?>
				</div>
				<?php
			}
			?>
		</div>
	</div>
	<!--BEGIN CONTENT-->
	<div class="page-content">
		<div id="tab-general">
			<div class="row mbl">
				<div class="col-lg-12">
					<ul class="nav nav-tabs nav-justified">
						<li class="active"><a data-toggle="tab" href="#sectionA"><h5><i class="fa fa-lock"></i>&nbsp;Change
									User
									Password</h5></a></li>
						<!--<li><a data-toggle="tab" href="#sectionB"><h5><i class="fa fa-user"></i>&nbsp;User Avatar</h5></a>
						</li>-->
						<!--<li><a data-toggle="tab" href="#sectionC"><h5><i class="fa fa-user-secret"></i>&nbsp;Admin Details</h5></a>
						</li>-->
					</ul>
					<div class="tab-content">
						<div id="sectionA" class="tab-pane fade in active">
							<hr/>
							<div class="col-md-12">
								<!-- Advanced Tables -->
								<?php if (($this->session->userdata('role') === "Admin") OR ($this->session->userdata('role')
									=== "Finance") OR ($this->session->userdata('role') === "Admin2")
								) { ?>
								<div class="panel panel-pink">
									<?php } ?>
									<?php if (($this->session->userdata('role') === "Agent")) { ?>
									<div class="panel panel-green">
										<?php } ?>
										<div class="panel-heading">
											<div class="row">
												<div class="col-sm-6">
													<h3>Change User Passwords</h3>
												</div>
											</div>
										</div>
										<div class="panel-body">
											<?php $this->load->helper('form'); ?>
											<?php echo form_open('systemUsers/changePassword'); ?>
											<div class="form-body pal">
												<div class="row">
													<div class="col-md-12">
														<div class="form-group">
															<label>Current Password</label>
															<?php echo form_password(array(
																"class" => "form-control",
																"placeholder" => "Enter your current password",
																"name" => "currentpass",
																"required" => "true"
															)) ?>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-12">
														<div class="form-group">
															<label>New Password</label>
															<?php echo form_password(array(
																"class" => "form-control",
																"placeholder" => "Enter your new password",
																"name" => "newpass",
																"required" => "true"
															)) ?>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-12">
														<div class="form-group">
															<label>Confirm Password</label>
															<?php echo form_password(array(
																"class" => "form-control",
																"placeholder" => "Confirm the new password",
																"name" => "confirmpass",
																"required" => "true"
															)) ?>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-4">
													</div>
													<div class="col-md-4">
														<div class="form-group">
														</div>
													</div>
													<div class="col-md-4">
														<div class="form-group">
															<button type="reset" class="btn btn-danger pull-right">Cancel
															</button>
															<?php echo form_submit('save', 'Change Password',
																'class="btn btn-success pull-right margin-right"'); ?>
														</div>
													</div>
												</div>
											</div>
											</form>
											<!-- /.row (nested) -->
										</div>
										<!-- /.panel-body -->
									</div>
									<!-- /.panel -->
								</div>
							</div>
							<div id="sectionB" class="tab-pane fade">
								<hr/>
								<div class="col-md-12">
									<!-- Advanced Tables -->
									<?php if (($this->session->userdata('role') === "Admin") OR ($this->session->userdata('role')
										=== "Finance") OR ($this->session->userdata('role') === "Admin2")
									) { ?>
									<div class="panel panel-pink">
										<?php } ?>
										<?php if (($this->session->userdata('role') === "Agent")) { ?>
										<div class="panel panel-green">
											<?php } ?>
											<div class="panel-heading">
												<div class="row">
													<div class="col-sm-6">
														<h3>Upload User Avatar</h3>
													</div>
												</div>
											</div>
											<div class="panel-body">
												<?php $this->load->helper('form'); ?>
												<form role="form" class="form-horizontal"
												      action="changeAvatars"
												      enctype='multipart/form-data' id="avatars">
													<div class="form-group">
														<label class="col-sm-3 control-label">User avatar</label>

														<div class="col-sm-6">
															<div class="fileinput fileinput-new" data-provides="fileinput">
																<div>
                                                                    <span class="btn btn-primary btn-file"><span
		                                                                    class="fileinput-new">Select image</span>
                                                                    <span class="fileinput-exists">Change</span><input
		                                                                    type="file" name="useravatar"></span>
																</div>
															</div>
														</div>
													</div>

													<button class="btn btn-default btn-flat btn-primary bg" type="submit">
														<span>Save Changes</span></button>


												</form>
												<!-- /.row (nested) -->
											</div>
											<!-- /.panel-body -->
										</div>
										<!-- /.panel -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--END CONTENT-->
