
DROP TABLE IF EXISTS `agents`;
CREATE TABLE `agents` (
  `Id` int(11) NOT NULL,
  `AgName` varchar(45) NOT NULL,
  `IDNo` varchar(45) NOT NULL,
  `Location` varchar(45) NOT NULL,
  `Number` varchar(45) NOT NULL,
  `Telephone` varchar(45) NOT NULL,
  `Email` varchar(70) NOT NULL,
  `AddedBy` varchar(80) NOT NULL,
  `UpdatedBy` varchar(80) DEFAULT NULL,
  `dateadded` date NOT NULL,
  `dateupdated` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `agents`
--

INSERT INTO `agents` (`Id`, `AgName`, `IDNo`, `Location`, `Number`, `Telephone`, `Email`, `AddedBy`, `UpdatedBy`, `dateadded`, `dateupdated`) VALUES
(1, 'Churchblaze Investment Limited', '67984523', 'Thika Road', 'CBA/34760/2015', '0701836798', 'info@churchblazegroup.com', 'Super User User', NULL, '2015-08-29', NULL),
(2, 'Api-Craft Technology', '29379856', 'Kenya', 'CBA/63041/2015', '0712953938', 'benjaminmwalimu@gmail.com', 'Super User User', NULL, '2015-08-30', NULL),
(3, 'd;lfbvjlnldf', '6576547', 'chuka', 'CBA/70289/2015', '0712455673', 'fshgdfhjd@gmail.com', 'Super User User', NULL, '2015-10-24', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `nextofkin`
--

DROP TABLE IF EXISTS `nextofkin`;
CREATE TABLE `nextofkin` (
  `kinId` int(11) NOT NULL,
  `kinName` varchar(45) NOT NULL,
  `kinIdentity` varchar(60) NOT NULL,
  `kinPhonenumber` varchar(45) NOT NULL DEFAULT 'N/A',
  `email` varchar(90) NOT NULL DEFAULT 'N/A',
  `percentage` int(11) NOT NULL DEFAULT '0',
  `relation` varchar(70) NOT NULL,
  `kinTo` int(11) NOT NULL,
  `KintoName` varchar(120) NOT NULL,
  `kintoidnumber` int(20) NOT NULL,
  `AddedBy` varchar(80) NOT NULL,
  `UpdatedBy` varchar(80) DEFAULT NULL,
  `dateadded` date NOT NULL,
  `dateupdated` date DEFAULT NULL,
  `physicalFormNumber` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nextofkin`
--

INSERT INTO `nextofkin` (`kinId`, `kinName`, `kinIdentity`, `kinPhonenumber`, `email`, `percentage`, `relation`, `kinTo`, `KintoName`, `kintoidnumber`, `AddedBy`, `UpdatedBy`, `dateadded`, `dateupdated`, `physicalFormNumber`) VALUES
(1, 'Benjamin Mwalimu', '29379856', '0712953938', 'benjaminmwalimu1@gmail.com', 50, 'son', 1, 'Mulyungi Benjamin Mwalimu', 29379856, 'Super User User', 'Super User User', '2015-10-17', '2015-10-17', 'fdgvfdcgvdcxvfd'),
(2, 'Peace Museo', 'djbf3894hhv', 'N/A', 'N/A', 30, 'Sister', 1, 'Mulyungi Benjamin Mwalimu', 29379856, 'Super User User', NULL, '2015-10-17', NULL, 'kfdlchvkbdfc342'),
(3, 'fghvchgfb', '54345656', '0712455673', 'N/A', 50, 'son', 5, 'Ronoh', 24965121, 'Super User User', NULL, '2015-10-24', NULL, 'kjgkhgkjgkj');

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
CREATE TABLE `payment` (
  `Id` int(11) NOT NULL,
  `PaymentUUID` varchar(255) NOT NULL,
  `shareType` int(11) NOT NULL,
  `Amount` int(20) NOT NULL,
  `numberofshares` int(60) NOT NULL,
  `Receiver` varchar(90) NOT NULL DEFAULT '',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `holder` int(11) DEFAULT NULL,
  `holderName` varchar(80) NOT NULL,
  `Code` varchar(255) NOT NULL DEFAULT 'N/A on Cash',
  `PaymentType` varchar(45) NOT NULL,
  `buyId` int(11) NOT NULL DEFAULT '0',
  `buyreceipt` varchar(60) NOT NULL,
  `idnumber` varchar(40) DEFAULT NULL,
  `AddedBy` varchar(80) NOT NULL,
  `UpdatedBy` varchar(80) DEFAULT NULL,
  `dateadded` date NOT NULL,
  `dateupdated` date DEFAULT NULL,
  `physicalFormNumber` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`Id`, `PaymentUUID`, `shareType`, `Amount`, `numberofshares`, `Receiver`, `date`, `holder`, `holderName`, `Code`, `PaymentType`, `buyId`, `buyreceipt`, `idnumber`, `AddedBy`, `UpdatedBy`, `dateadded`, `dateupdated`, `physicalFormNumber`) VALUES
(1, 'CBP/5348/2015', 1, 5000, 4, 'Super User User', '2015-08-29 08:53:31', 1, 'Mulyungi Benjamin Mwalimu', 'N/A on Cash Payment', 'Cash', 1, 'CBB/04518/2015', '29379856', 'Super User User', NULL, '2015-08-29', NULL, ''),
(2, 'CBP/5948/2015', 2, 400000, 10000, 'Super User User', '2015-09-01 17:52:11', 1, 'Mulyungi Benjamin Mwalimu', 'N/A on Cash Payment', 'Cash', 3, 'CBB/27905/2015', '29379856', 'Super User User', '0', '2015-09-01', '2015-10-18', 'dfscxvd fcx4546345'),
(3, 'CBP/5128/2015', 2, 40000, 1000, 'Super User User', '2015-09-05 09:26:41', 1, 'Mulyungi Benjamin Mwalimu', 'N/A on Cash Payment', 'Cash', 6, 'CBB/42639/2015', '29379856', 'Super User User', NULL, '2015-09-05', NULL, ''),
(4, 'CBP/7501/2015', 2, 80000, 2000, 'Super User User', '2015-09-08 16:21:15', 1, 'Mulyungi Benjamin Mwalimu', 'N/A on Cash Payment', 'Cash', 3, 'CBB/27905/2015', '29379856', 'Super User User', NULL, '2015-09-08', NULL, ''),
(5, 'CBP/6708/2015', 2, 80000, 2000, 'Super User User', '2015-09-08 16:22:03', 1, 'Mulyungi Benjamin Mwalimu', 'N/A on Cash Payment', 'Cash', 3, 'CBB/27905/2015', '29379856', 'Super User User', NULL, '2015-09-08', NULL, ''),
(6, 'CBP/5982/2015', 2, 80000, 2000, 'Super User User', '2015-09-08 16:22:47', 1, 'Mulyungi Benjamin Mwalimu', 'N/A on Cash Payment', 'Cash', 3, 'CBB/27905/2015', '29379856', 'Super User User', NULL, '2015-09-08', NULL, ''),
(7, 'CBP/8279/2015', 2, 80000, 2000, 'Super User User', '2015-09-08 16:25:06', 1, 'Mulyungi Benjamin Mwalimu', 'N/A on Cash Payment', 'Cash', 3, 'CBB/27905/2015', '29379856', 'Super User User', NULL, '2015-09-08', NULL, ''),
(8, 'CBP/0387/2015', 2, 80000, 2000, 'Super User User', '2015-09-08 16:26:07', 1, 'Mulyungi Benjamin Mwalimu', 'N/A on Cash Payment', 'Cash', 3, 'CBB/27905/2015', '29379856', 'Super User User', NULL, '2015-09-08', NULL, ''),
(9, 'CBP/8267/2015', 2, 80000, 2000, 'Super User User', '2015-09-08 16:27:21', 1, 'Mulyungi Benjamin Mwalimu', 'N/A on Cash Payment', 'Cash', 3, 'CBB/27905/2015', '29379856', 'Super User User', NULL, '2015-09-08', NULL, ''),
(10, 'CBP/1957/2015', 2, 80000, 2000, 'Super User User', '2015-09-08 16:27:36', 1, 'Mulyungi Benjamin Mwalimu', 'N/A on Cash Payment', 'Cash', 3, 'CBB/27905/2015', '29379856', 'Super User User', NULL, '2015-09-08', NULL, ''),
(11, 'CBP/3924/2015', 2, 40000, 1000, 'Super User User', '2015-09-14 16:05:49', 1, 'Mulyungi Benjamin Mwalimu', 'N/A on Cash Payment', 'Cash', 7, 'CBB/69415/2015', '29379856', 'Super User User', NULL, '2015-09-14', NULL, ''),
(12, 'CBP/8043/2015', 2, 40000, 1000, 'Super User User', '2015-10-02 06:02:01', 1, 'Mulyungi Benjamin Mwalimu', 'N/A on Cash Payment', 'Cash', 8, 'CBB/04367/2015', '29379856', 'Super User User', NULL, '2015-10-02', NULL, ''),
(13, 'CBP/9143/2015', 2, 40000, 1000, 'Super User User', '2015-10-17 22:29:57', 1, 'Mulyungi Benjamin Mwalimu', 'N/A on Cash Payment', 'Cash', 10, 'CBB/09186/2015', '29379856', 'Super User User', NULL, '2015-10-18', NULL, ''),
(14, 'CBP/0729/2015', 2, 9120, 228, 'Super User User', '2015-10-18 01:29:08', 1, 'Mulyungi Benjamin Mwalimu', 'N/A on Cash Payment', 'Cash', 3, 'CBB/27905/2015', '29379856', 'Super User User', NULL, '2015-10-18', NULL, 'odhxckjvkdjx2343'),
(15, 'CBP/1578/2015', 2, 40000, 1000, 'Super User User', '2015-10-24 10:21:48', 5, 'Ronoh', 'N/A on Cash Payment', 'Cash', 12, 'CBB/04285/2015', '24965121', 'Super User User', NULL, '2015-10-24', NULL, 'fddgfdgdff');

-- --------------------------------------------------------

--
-- Table structure for table `regpay`
--

DROP TABLE IF EXISTS `regpay`;
CREATE TABLE `regpay` (
  `payId` int(11) NOT NULL,
  `shareholder` varchar(90) NOT NULL,
  `shareholderId` int(11) DEFAULT NULL,
  `idnumber` varchar(80) NOT NULL,
  `amount` int(11) NOT NULL,
  `receipt` varchar(90) NOT NULL,
  `paymentmode` varchar(60) NOT NULL,
  `code` varchar(120) DEFAULT NULL,
  `datepaid` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `receiver` varchar(90) NOT NULL,
  `AddedBy` varchar(80) DEFAULT NULL,
  `UpdatedBy` varchar(80) DEFAULT NULL,
  `dateadded` date DEFAULT NULL,
  `dateupdated` date DEFAULT NULL,
  `physicalFormNumber` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `regpay`
--

INSERT INTO `regpay` (`payId`, `shareholder`, `shareholderId`, `idnumber`, `amount`, `receipt`, `paymentmode`, `code`, `datepaid`, `receiver`, `AddedBy`, `UpdatedBy`, `dateadded`, `dateupdated`, `physicalFormNumber`) VALUES
(2, 'Mulyungi Benjamin Mwalimu', 1, '29379856', 1000, 'REG/7608/2015', 'Cash', 'N/A for Cash Payment', '2015-09-05 09:22:00', 'Super User User', 'Super User User', NULL, '2015-09-05', '2015-10-18', 'Demo754687'),
(4, 'Delivarance Church Sytem', 4, '29379856', 1250, 'REG/4206/2015', 'Cash', 'N/A for Cash Payment', '2015-10-24 09:14:03', 'Super User User', 'Super User User', NULL, '2015-10-24', NULL, 'dfglnbdkgvjfnkj'),
(5, 'Ronoh', 5, '24965121', 1250, 'REG/5840/2015', 'M-Pesa', 'ghgfvcgbvdfcxhb fg', '2015-10-24 09:48:28', 'Super User User', 'Super User User', NULL, '2015-10-24', NULL, 'htgfhtrgfvhnfgvb');

-- --------------------------------------------------------

--
-- Table structure for table `regpaysetup`
--

DROP TABLE IF EXISTS `regpaysetup`;
CREATE TABLE `regpaysetup` (
  `rpsId` int(11) NOT NULL,
  `amountPay` int(11) NOT NULL,
  `AddedBy` varchar(80) DEFAULT NULL,
  `UpdatedBy` varchar(80) DEFAULT NULL,
  `dateadded` date DEFAULT NULL,
  `dateupdated` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `regpaysetup`
--

INSERT INTO `regpaysetup` (`rpsId`, `amountPay`, `AddedBy`, `UpdatedBy`, `dateadded`, `dateupdated`) VALUES
(3, 1000, 'Super User User', NULL, '2015-10-25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `shareholders`
--

DROP TABLE IF EXISTS `shareholders`;
CREATE TABLE `shareholders` (
  `Id` int(11) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `Name` varchar(90) NOT NULL,
  `IdNo` varchar(11) DEFAULT NULL,
  `Telephone` varchar(20) NOT NULL,
  `Email` varchar(70) DEFAULT NULL,
  `Box` varchar(45) DEFAULT NULL,
  `Village` varchar(45) NOT NULL,
  `Town` varchar(45) NOT NULL,
  `County` varchar(45) NOT NULL,
  `Country` varchar(45) NOT NULL,
  `cin` varchar(90) DEFAULT NULL,
  `krapin` varchar(90) DEFAULT NULL,
  `Approval` int(1) NOT NULL DEFAULT '0',
  `formnumber` varchar(80) DEFAULT 'None',
  `agentID` int(11) DEFAULT '0',
  `activatedBy` varchar(90) DEFAULT NULL,
  `dateActivated` date DEFAULT NULL,
  `deactivatedBy` varchar(90) DEFAULT NULL,
  `dateDeactivated` date DEFAULT NULL,
  `AddedBy` varchar(80) NOT NULL,
  `UpdatedBy` varchar(80) DEFAULT NULL,
  `dateadded` date NOT NULL,
  `dateupdated` date DEFAULT NULL,
  `physicalFormNumber` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shareholders`
--

INSERT INTO `shareholders` (`Id`, `type`, `Name`, `IdNo`, `Telephone`, `Email`, `Box`, `Village`, `Town`, `County`, `Country`, `cin`, `krapin`, `Approval`, `formnumber`, `agentID`, `activatedBy`, `dateActivated`, `deactivatedBy`, `dateDeactivated`, `AddedBy`, `UpdatedBy`, `dateadded`, `dateupdated`, `physicalFormNumber`) VALUES
(1, 0, 'Mulyungi Benjamin Mwalimu', '29379856', '+254712953938', 'benjaminmwalimu1@gmail.com', '21461', 'Musengo', 'Nairobi', 'Kitui', 'Kenya', NULL, NULL, 1, 'FORM/456903/2015', 1, 'Super User User', '2015-09-05', 'Super User User', '2015-09-05', 'Super User User', NULL, '2015-08-29', NULL, 'fdcvfdcvdfc'),
(2, 1, 'Housing Management System', NULL, '0701836798', 'mulyungi.benjamin@students.ku.ac.ke', '21461', 'Nairobi', 'Nairobi', 'Nairobi', 'Kenya', 'dfcgerter234324', '43344efdcd', 0, 'None', 1, NULL, NULL, NULL, NULL, 'Super User User', 'Super User User', '2015-08-29', '2015-10-25', 'kdsgfjhgfdhj972346'),
(3, 0, 'Tabitha Mulyungi', '9110887', '+254712953938', 'tabbylyungi@gmail.com', '21461', 'Musengo', 'Kitui', 'Kitui', 'Kenya', NULL, NULL, 1, 'FORM/530627/2015', 2, 'Super User User', '2015-10-17', 'Super User User', '2015-09-05', 'Super User User', NULL, '2015-08-30', NULL, ''),
(4, 0, 'Delivarance Church Sytem', '29379856', '0712953938', 'dliver@gmail.com', '21461-00505', 'Musengo', 'Kitui', 'Kitui', 'Kenya', NULL, NULL, 0, 'None', 1, NULL, NULL, NULL, NULL, 'Super User User', 'Super User User', '2015-10-17', '2015-10-25', 'dfgfdgbfdbf33'),
(5, 0, 'Ronoh', '24965121', '0712455673', 'kronoh25@gmail.com', '534356', 'roysambu', 'Nairobi', 'Nairobi', 'Kenya', NULL, NULL, 1, 'FORM/683504/2015', 1, 'Super User User', '2015-10-24', NULL, NULL, 'Super User User', NULL, '2015-10-24', NULL, 'dfglnbdkgvjfnkj');

-- --------------------------------------------------------

--
-- Table structure for table `sharesBought`
--

DROP TABLE IF EXISTS `sharesBought`;
CREATE TABLE `sharesBought` (
  `buyId` int(11) NOT NULL,
  `shareholder` int(11) NOT NULL,
  `idnumber` varchar(30) DEFAULT NULL,
  `sharetype` int(11) NOT NULL,
  `shareTypeName` varchar(60) NOT NULL,
  `shareNumber` int(11) NOT NULL,
  `expectedAmount` int(12) NOT NULL DEFAULT '0',
  `Receiptnumber` varchar(70) NOT NULL,
  `datebought` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `AddedBy` varchar(80) NOT NULL,
  `UpdatedBy` varchar(80) DEFAULT NULL,
  `dateadded` date NOT NULL,
  `dateupdated` date DEFAULT NULL,
  `physicalFormNumber` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sharesBought`
--

INSERT INTO `sharesBought` (`buyId`, `shareholder`, `idnumber`, `sharetype`, `shareTypeName`, `shareNumber`, `expectedAmount`, `Receiptnumber`, `datebought`, `AddedBy`, `UpdatedBy`, `dateadded`, `dateupdated`, `physicalFormNumber`) VALUES
(1, 1, '29379856', 1, 'Series A', 2, 2500, 'CBB/04518/2015', '2015-08-29 08:52:34', 'Super User User', 'Super User User', '2015-08-29', '2015-09-01', ''),
(2, 1, '29379856', 1, 'Series A', 2, 2500, 'CBB/84360/2015', '2015-08-29 10:43:59', 'Super User User', NULL, '2015-08-29', NULL, ''),
(3, 1, '29379856', 2, 'Series B', 10000, 400000, 'CBB/27905/2015', '2015-09-01 17:47:31', 'Super User User', 'Super User User', '2015-09-01', '2015-10-18', 'dfgfdgbfdbf33'),
(4, 1, '29379856', 1, 'Series A', 2, 2500, 'CBB/78456/2015', '2015-09-01 18:13:45', 'Super User User', NULL, '2015-09-01', NULL, ''),
(5, 1, '29379856', 2, 'Series B', 25000, 1000000, 'CBB/28306/2015', '2015-09-01 18:19:04', 'Super User User', NULL, '2015-09-01', NULL, ''),
(6, 1, '29379856', 2, 'Series B', 2000, 80000, 'CBB/42639/2015', '2015-09-05 09:25:08', 'Super User User', NULL, '2015-09-05', NULL, ''),
(7, 1, '29379856', 2, 'Series B', 3000, 120000, 'CBB/69415/2015', '2015-09-14 16:02:24', 'Super User User', NULL, '2015-09-14', NULL, ''),
(8, 1, '29379856', 2, 'Series B', 1000, 40000, 'CBB/04367/2015', '2015-09-14 16:10:07', 'Super User User', NULL, '2015-09-14', NULL, ''),
(9, 1, '29379856', 2, 'Series B', 1000, 40000, 'CBB/71643/2015', '2015-10-02 06:01:22', 'Super User User', NULL, '2015-10-02', NULL, ''),
(10, 1, '29379856', 2, 'Series B', 2000, 80000, 'CBB/09186/2015', '2015-10-17 22:22:27', 'Super User User', NULL, '2015-10-18', NULL, 'dfgfdgbfdbf33'),
(11, 5, '24965121', 2, 'Series B', 4, 160, 'CBB/43987/2015', '2015-10-24 09:57:27', 'Super User User', NULL, '2015-10-24', NULL, 'dfglnbdkgvjfnkj'),
(12, 5, '24965121', 2, 'Series B', 1000, 40000, 'CBB/04285/2015', '2015-10-24 10:18:46', 'Super User User', NULL, '2015-10-24', NULL, 'fdgdfgdfsg'),
(13, 1, '29379856', 2, 'Series B', 2000, 80000, 'CBB/91735/2015', '2015-10-25 12:49:45', 'Super User User', NULL, '2015-10-25', NULL, 'dfcgfc4543');

-- --------------------------------------------------------

--
-- Table structure for table `sharetypes`
--

DROP TABLE IF EXISTS `sharetypes`;
CREATE TABLE `sharetypes` (
  `Id` int(11) NOT NULL,
  `type` varchar(45) NOT NULL,
  `seriesPrice` int(11) NOT NULL,
  `sharetotal` int(12) NOT NULL DEFAULT '0',
  `sharesold` int(12) NOT NULL DEFAULT '0',
  `amountexpected` int(12) NOT NULL DEFAULT '0',
  `amountreceived` int(12) NOT NULL DEFAULT '0',
  `shareAmountExpected` int(11) DEFAULT NULL,
  `Description` varchar(200) NOT NULL,
  `AddedBy` varchar(80) NOT NULL,
  `UpdatedBy` varchar(80) DEFAULT NULL,
  `dateadded` date NOT NULL,
  `dateupdated` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sharetypes`
--

INSERT INTO `sharetypes` (`Id`, `type`, `seriesPrice`, `sharetotal`, `sharesold`, `amountexpected`, `amountreceived`, `shareAmountExpected`, `Description`, `AddedBy`, `UpdatedBy`, `dateadded`, `dateupdated`) VALUES
(1, 'Series A', 1250, 10, 6, 12500, 5000, 7500, 'Initial Shares', 'Super User User', NULL, '2015-08-29', NULL),
(2, 'Series B', 40, 100000, 47004, 4000000, 849120, 1880160, 'Second offer', 'Super User User', NULL, '2015-09-01', NULL),
(4, 'Series E', 100, 1000000, 0, 100000000, 0, NULL, 'kjdczvkxkfzhsjcbhjvhjchxkjl;v', 'Super User User', NULL, '2015-10-24', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

DROP TABLE IF EXISTS `staff`;
CREATE TABLE `staff` (
  `Id` int(11) NOT NULL,
  `Fname` varchar(45) NOT NULL,
  `Sname` varchar(45) NOT NULL,
  `Lname` varchar(45) NOT NULL,
  `WorkNumber` varchar(45) NOT NULL,
  `Department` varchar(45) NOT NULL,
  `Gender` varchar(45) NOT NULL,
  `DOB` date NOT NULL,
  `Mobile` varchar(45) NOT NULL,
  `Email` varchar(60) NOT NULL,
  `AddedBy` varchar(80) NOT NULL,
  `UpdatedBy` varchar(80) DEFAULT NULL,
  `dateadded` date NOT NULL,
  `dateupdated` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`Id`, `Fname`, `Sname`, `Lname`, `WorkNumber`, `Department`, `Gender`, `DOB`, `Mobile`, `Email`, `AddedBy`, `UpdatedBy`, `dateadded`, `dateupdated`) VALUES
(1, 'Super', 'User', 'User', 'CBA/001/2015', 'Technology', 'Male', '1980-05-05', '0712953938', 'super@gmail.com', 'Super User User', NULL, '2015-06-25', NULL),
(2, 'Esther', 'Mbaire', 'Wanjiku', 'CB15/017', 'IT', 'Male', '1978-09-09', '0728241324', 'mbaire@churchblaze.com', 'Super User User', 'Super User User', '2015-10-24', '2015-10-25'),
(4, 'Benjamin', 'Mwalimu', 'Mulyungi', 'CB15/981', 'Technology', 'Male', '1992-09-11', '0712953938', 'benjaminmwalimu1@gmail.com', 'Super User User', 'Super User User', '2015-10-25', '2015-10-25'),
(5, 'Elijah ', 'Meja', 'Mecha', 'CB15/981', 'Technology', 'Male', '1987-08-12', '0722628494', 'info2meja@gmail.com', 'Super User User', 'Super User User', '2015-10-25', '2015-10-25');

-- --------------------------------------------------------

--
-- Table structure for table `systeminfo`
--

DROP TABLE IF EXISTS `systeminfo`;
CREATE TABLE `systeminfo` (
  `Id` int(11) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `addressone` varchar(100) NOT NULL,
  `addresstwo` varchar(100) DEFAULT NULL,
  `phonenumberone` varchar(30) NOT NULL,
  `phonenumbertwo` varchar(30) DEFAULT NULL,
  `bankname` varchar(200) NOT NULL,
  `bankbranch` varchar(200) NOT NULL,
  `accountname` varchar(200) NOT NULL,
  `accountnumber` varchar(70) NOT NULL,
  `paybillnumber` varchar(30) NOT NULL,
  `AddedBy` varchar(80) NOT NULL,
  `dateadded` date NOT NULL,
  `UpdatedBy` varchar(80) DEFAULT NULL,
  `dateupdated` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `systeminfo`
--

INSERT INTO `systeminfo` (`Id`, `Name`, `addressone`, `addresstwo`, `phonenumberone`, `phonenumbertwo`, `bankname`, `bankbranch`, `accountname`, `accountnumber`, `paybillnumber`, `AddedBy`, `dateadded`, `UpdatedBy`, `dateupdated`) VALUES
(3, 'Churchblaze Investment Limited', 'P.o. Box 6146-00618', '', '+254701899251', '', 'Co-Operative Bank of Kenya', 'Thika Road Mall Branch', 'Churchblaze Group Limited', '01136575354300', '400200', 'Super User User', '2015-09-14', 'Super User User', '2015-10-24');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(60) NOT NULL,
  `avatar` varchar(1000) DEFAULT NULL,
  `role` varchar(50) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `datecreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `link` int(11) DEFAULT NULL,
  `link2` int(11) DEFAULT NULL,
  `link3` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `avatar`, `role`, `status`, `datecreated`, `link`, `link2`, `link3`) VALUES
(1, 'super', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'Admin', 1, '2015-06-25 16:39:36', NULL, 1, NULL),
(2, 'churchblaze', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'Agent', 1, '2015-08-28 22:34:24', 1, NULL, NULL),
(3, 'dubdabasoduba', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'Shareholder', 1, '2015-08-28 22:34:50', NULL, NULL, 1),
(4, '43344efdcd', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'Shareholder', 1, '2015-08-29 13:12:30', NULL, NULL, 2),
(5, 'apicraft', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'Agent', 1, '2015-08-29 23:16:23', 2, NULL, NULL),
(6, 'tabitha', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'Shareholder', 1, '2015-08-29 23:17:27', NULL, NULL, 3),
(7, '29379856', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'Shareholder', 1, '2015-10-17 12:23:07', NULL, NULL, 4),
(8, 'Esther.Wanjiku', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'Admin', 1, '2015-10-24 09:27:12', NULL, 2, NULL),
(9, 'kjfhghjvg', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'Agent', 1, '2015-10-24 09:30:52', 3, NULL, NULL),
(10, 'ronoh123', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'Shareholder', 1, '2015-10-24 09:45:50', NULL, NULL, 5),
(12, 'Benjamin.Mulyungi', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'Admin', 1, '2015-10-25 13:00:30', NULL, 4, NULL),
(13, 'Elijah .Mecha', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'Admin', 1, '2015-10-25 13:47:49', NULL, 5, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agents`
--
ALTER TABLE `agents`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `nextofkin`
--
ALTER TABLE `nextofkin`
  ADD PRIMARY KEY (`kinId`),
  ADD KEY `fk_nextofkin_1_idx` (`kinTo`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `fk_payment_1_idx` (`holder`),
  ADD KEY `fk_payment_2_idx` (`shareType`),
  ADD KEY `fk_payment_3_idx` (`buyId`);

--
-- Indexes for table `regpay`
--
ALTER TABLE `regpay`
  ADD PRIMARY KEY (`payId`),
  ADD KEY `fk_regpay_1_idx` (`shareholderId`);

--
-- Indexes for table `regpaysetup`
--
ALTER TABLE `regpaysetup`
  ADD PRIMARY KEY (`rpsId`);

--
-- Indexes for table `shareholders`
--
ALTER TABLE `shareholders`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `fk_shareholders_1_idx` (`agentID`);

--
-- Indexes for table `sharesBought`
--
ALTER TABLE `sharesBought`
  ADD PRIMARY KEY (`buyId`),
  ADD KEY `fk_sharesBought_1_idx` (`shareholder`),
  ADD KEY `fk_sharesBought_2_idx` (`sharetype`);

--
-- Indexes for table `sharetypes`
--
ALTER TABLE `sharetypes`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `systeminfo`
--
ALTER TABLE `systeminfo`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_users_2_idx` (`link2`),
  ADD KEY `fk_users_1_idx` (`link`),
  ADD KEY `fk_users_3_idx` (`link3`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agents`
--
ALTER TABLE `agents`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `nextofkin`
--
ALTER TABLE `nextofkin`
  MODIFY `kinId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `regpay`
--
ALTER TABLE `regpay`
  MODIFY `payId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `regpaysetup`
--
ALTER TABLE `regpaysetup`
  MODIFY `rpsId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `shareholders`
--
ALTER TABLE `shareholders`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sharesBought`
--
ALTER TABLE `sharesBought`
  MODIFY `buyId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `sharetypes`
--
ALTER TABLE `sharetypes`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `systeminfo`
--
ALTER TABLE `systeminfo`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `nextofkin`
--
ALTER TABLE `nextofkin`
  ADD CONSTRAINT `fk_nextofkin_1` FOREIGN KEY (`kinTo`) REFERENCES `shareholders` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `payment`
--
ALTER TABLE `payment`
  ADD CONSTRAINT `fk_payment_1` FOREIGN KEY (`holder`) REFERENCES `shareholders` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_payment_2` FOREIGN KEY (`shareType`) REFERENCES `sharetypes` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_payment_3` FOREIGN KEY (`buyId`) REFERENCES `sharesBought` (`buyId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `regpay`
--
ALTER TABLE `regpay`
  ADD CONSTRAINT `fk_regpay_1` FOREIGN KEY (`shareholderId`) REFERENCES `shareholders` (`Id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `shareholders`
--
ALTER TABLE `shareholders`
  ADD CONSTRAINT `fk_shareholders_1` FOREIGN KEY (`agentID`) REFERENCES `agents` (`Id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `sharesBought`
--
ALTER TABLE `sharesBought`
  ADD CONSTRAINT `fk_sharesBought_1` FOREIGN KEY (`shareholder`) REFERENCES `shareholders` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_sharesBought_2` FOREIGN KEY (`sharetype`) REFERENCES `sharetypes` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_users_1` FOREIGN KEY (`link`) REFERENCES `agents` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_users_2` FOREIGN KEY (`link2`) REFERENCES `staff` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_users_3` FOREIGN KEY (`link3`) REFERENCES `shareholders` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;
