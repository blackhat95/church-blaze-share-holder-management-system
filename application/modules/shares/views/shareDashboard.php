<!-- /. NAV SIDE  -->
<div id="page-wrapper">
	<div id="page-inner">
		<!--BEGIN TITLE & BREADCRUMB PAGE-->
		<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
			<div class="page-header pull-left">
				<div class="page-title">
					Shares Management
				</div>
			</div>
			<ol class="breadcrumb page-breadcrumb pull-right">
				<li><i class="fa fa-home"></i>&nbsp;<a href="<?php echo base_url() . "home" ?>">Home</a>&nbsp;&nbsp;
				</li>
				<li><i class="fa fa-stack-exchange"></i>&nbsp;&nbsp;Shares Management</li>
			</ol>
			<div class="clearfix">
			</div>
		</div>
		<!--END TITLE & BREADCRUMB PAGE-->
		<div class="row">
			<div class="alert alert-info">
				<i class="fa fa-info-circle"></i>
				<strong>Heads up!</strong>
				This panel allow to access the share subscription functionality
			</div>
			<hr/>
			<ul class="nav nav-tabs nav-justified">
				<li class="active"><a data-toggle="tab" href="#sharessubscription"><h5><i class="fa fa-stack-exchange"></i>&nbsp;
							Shares Subscription</h5></a></li>
			</ul>
			<div class="tab-content">
				<div id="sharessubscription" class="tab-pane fade in active">
					<hr/>
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="panel panel-primary text-center no-boder bg-color-brown">
							<div class="panel-body">

								<a href='<?php echo base_url() . 'shares/buyShares' ?>'><i
											class="fa fa-stack-exchange fa-10x bg-color-brown"></i></a>

								<h3></h3>
							</div>
							<div class="panel-footer back-footer-brown">
								<a class="white" href='<?php echo base_url() . 'shares/buyShares' ?>'>Subscribed Shares</a>
							</div>
						</div>
					</div>
					<?php
					if (($this->session->userdata('role') === "Admin") OR ($this->session->userdata('role') === "Admin2")
							OR ($this->session->userdata('role') === "Finance")
					) {
						?>
						<div class="col-md-6 col-sm-12 col-xs-12">
							<div class="panel panel-primary text-center no-boder bg-color-blue">
								<div class="panel-body">
									<a href='<?php echo base_url() . 'shares/viewShares' ?>'><i
												class="fa fa-edit fa-10x bg-color-blue"></i></a>
									
									<h3></h3>
								</div>
								<div class="panel-footer back-footer-blue">
									<a class="white"
									   href='<?php echo base_url() . 'shares/viewShares' ?>'>View
										Subscribed Shares</a>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>



